### Changelog

All notable changes to this project will be documented in this file. Dates are displayed in UTC.

Adapted from [`auto-changelog`](https://github.com/CookPete/auto-changelog).

#### [Unreleased](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag/compare/v1.4.0...HEAD)

#### [v1.4.0](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag/compare/v1.3.0...v1.4.0)

> 30 May 2022

⚠ **CONFIG FORMAT HAS CHANGED !**

Integrate @sdaste's new data files

See merge request [`!7`](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag/merge_requests/7)

- Added red channel to NWB file [`#66`](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag/-/issues/66)
- Check empty config fields [`#70`](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag/-/issues/70)
- Implemented an experiment version pattern [`#68`](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag/-/issues/68)
- Reference ROIs subset for each plane [`#67`](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag/-/issues/67)
- Added units for acquisition timeseries [`#44`](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag/-/issues/44)
- Added mandatory fields in config [`#69`](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag/-/issues/69)
- Replace Suite2p's dummy imaging metadata by the config's metadata [`#62`](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag/-/issues/62)
- Support @sdaste Teensy version [`#64`](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag/-/issues/64)

#### [v1.3.0](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag/compare/v1.2.0...v1.3.0)

> 29 September 2021

Timestamps improvements

Closes [`#39`](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag/-/issues/39), [`#56`](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag/-/issues/56), [`#59`](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag/-/issues/59), [`#48`](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag/-/issues/48), and [`#49`](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag/-/issues/49)

See merge request [`!6`](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag/merge_requests/6)

- Aligned the Teensy trials on the XML [`#59`](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag/-/issues/59)
- Refactored timestamps using `absoluteTime` from the XML file
- Adapted Teensy parser for spontaneous activity

#### [v1.2.0](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag/compare/v1.1.0...v1.2.0)

> 17 June 2021

#### [v1.1.0](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag/compare/v1.0.0...v1.1.0)

> 10 January 2021

#### [v1.0.0](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag/compare/v1.0.0-beta.2...v1.0.0)

> 6 November 2020

#### [v1.0.0-beta.2](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag/compare/v1.0.0-beta.1...v1.0.0-beta.2)

> 30 October 2020

#### [v1.0.0-beta.1](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag/compare/v1.0.0-beta...v1.0.0-beta.1)

> 23 July 2020

#### [v1.0.0-beta](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag/compare/v1.0.0-alpha...v1.0.0-beta)

> 30 June 2020

#### [v1.0.0-alpha](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag/compare/v0.1.0...v1.0.0-alpha)

> 20 June 2020

#### v0.1.0

> 2 April 2020
