# Calimag

[![pipeline status](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag/badges/master/pipeline.svg)](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag/-/commits/master)
[![coverage report](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag/badges/master/coverage.svg?job=test)](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag/-/commits/master)
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

CLI tool to convert raw calcium imaging data to the NWB standard file format

## Table of Contents

<!-- - [Background](#background) -->

- [Overview](#overview)
- [Install](#install)
- [Usage](#usage)
- [Development](#development)

<!-- ## Background -->

## Overview

Here's a video demonstrating how to use the tool from the command line:

![Demo video](docs/demo.ogv)

The resulting NWB file will look like the following:

![NWB output](docs/nwb-output.png)

You can use [HDFView](https://www.hdfgroup.org/downloads/hdfview/) to easily browse inside the NWB file. The NWB file produced should also be easily shareable on [DANDI](https://dandiarchive.org/) without modifications.

## Install

Download [this `environment.yml` file](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag/-/blob/master/environment.yml) to your local machine and replace the last 2 lines by the following:

```yaml
- pip:
    - git+https://gitlab.com/fleischmann-lab/calcium-imaging/calimag.git
```

Install this environment with: `conda env create -f environment.yml` and activate it with: `conda activate calimag`

Update environment with: `conda env update -f environment.yml` when there are changes.

## Usage

### Configuration

Download [this configuration file](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag/-/blob/master/tests/user.config.toml) to your local machine and replace the file paths and the metadata by your own values.

### Experiment version

The allowed experiment versions labels are defined in the [`constants.py` file at the `EXPERIMENT_VERSIONS` key](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag/-/blob/master/src/calimag/constants.py).

### Convert your files to NWB

Run the following command in your terminal:

```bash
calimag "/path/to/your/config/file.toml"
```

## Development

### Setup

To set up a development installation, first install the HDF5 binaries. The right HDF5 version to install is specified in the [`environment.yml`](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag/-/blob/master/environment.yml) file. Then clone the repository and install the development dependencies:

```shell
git clone https://gitlab.com/fleischmann-lab/calcium-imaging/calimag.git
cd calimag
python -m venv .venv
source .venv/bin/activate
pip install -e .[dev]
```

### Testing

The CI uses [GNU Make](https://www.gnu.org/software/make/) which can be installed by running the following commands:

- Linux: `sudo apt install make` (depends on the distribution/package manager)
- MacOS: `brew install make`, the command should then be available as `gmake` instead of `make` (https://formulae.brew.sh/formula/make)
- Windows: `choco install make` (https://community.chocolatey.org/packages/make)

Check that all the tests are passing by running `make test`. If everything is green ✔, your development installation should be ready!

### Linting/Formatting

You can autoformat some files by running `make format` (need `prettier` from `npm` if using `make prettier-format`), and you can check for the linting CI job before pushing by running `make lint-python` or `make lint-all`.

### Precommit hooks

To activate the pre-commit hook, while `.venv` is activated, you can run `pre-commit install` once. As of now, the hook is only for lint checks. For commits that you do not want the hook to take effect, do `git commit --no-verify`.
