"""
Packaging script.

All info is in `setup.cfg`
"""
import setuptools

# with open(dependencies_file) as fid:
#     dependencies = fid.read().splitlines()

if __name__ == "__main__":
    # setuptools.setup(install_requires=dependencies)
    setuptools.setup(
        use_scm_version=True,
    )
