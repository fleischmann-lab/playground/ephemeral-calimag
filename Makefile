##
# Calimag
#
# @file
# @version 0.1

help: ## Show this help
	@egrep -h '\s##\s' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

black-check:
	black --check .

black-format:
	black .

isort-check:
	isort --check .

isort-format:
	isort .

flake8:
	flake8 .

pydocstyle:
	pydocstyle .

mypy:
	mypy -p calimag --lineprecision-report .
	cat lineprecision.txt

codespell:
	codespell

prettier-check:
	prettier --check "**/*.{yaml,yml}"
	prettier --check "**/*.md"

prettier-format:
	prettier --write "**/*.{yaml,yml}"
	prettier --write "**/*.md"

format: ## Autoformat everything
	make isort-format
	make black-format
	make prettier-format

lint-python: ## Run Python based static analysis
	make flake8
	make isort-check
	make pydocstyle
	make black-check
	make mypy
	make codespell

lint-all: ## Run all static analysis
	make lint-python
	make prettier-check

test: ## Run tests
	pytest --cov=calimag -n auto tests/ --durations=10 -v

# TODO: to be refractored or removed when issue #71 is solved
test-xml: ## Run tests with XML coverage
	pytest --cov=calimag -n 2 tests/ --durations=10 -v || \
		pytest --cov=calimag --cov-append --lf -n 1 tests/ --durations=10 -v
	coverage report
	coverage xml

test-html: ## Run tests with HTML report
	pytest --cov=calimag --cov-report html -n auto tests/ --durations=10 -v

clean: ## Clean tmp files
	find . -type f -name *.pyc -delete
	find . -type d -name __pycache__ -delete

guix-shell: ## Spawn a isolated shell
	guix shell -m manifest.scm --pure
# end
