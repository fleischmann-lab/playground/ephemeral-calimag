import datetime
import xml.etree.ElementTree as ET
from pathlib import Path
from unittest.mock import mock_open, patch

import numpy as np
import pandas as pd
import pytest
import test_constants as TEST_CST
from hypothesis import given
from hypothesis import strategies as st
from packaging import version

import calimag.constants as CONST
import calimag.errors as err
from calimag.parsers import (
    MicroscopeParser,
    TeensyParser,
    create_aligned_timestamps,
    get_timedelta_from_start_time,
)


@pytest.mark.parametrize(
    "filepath, frame_index, frame_count, frame_time_delta,"
    "trial2_idx, time_between_trials, expected_time_frames",
    [
        (
            TEST_CST.XML_FILEPATH[0],
            1,
            11990,
            pd.Timedelta(137718802, "ns"),
            218,
            60.005,
            [
                pd.Timestamp("2019-07-10T18:13:11.065433400", tz="EST"),
                pd.Timestamp("2019-07-10 18:13:11.203152202", tz="EST"),
                pd.Timestamp("2019-07-10 18:14:11.070433400", tz="EST"),
                pd.Timestamp("2019-07-10T19:07:40.986331408", tz="EST"),
            ],
        ),
        (
            TEST_CST.XML_FILEPATH[0],
            2,
            11990,
            pd.Timedelta(137718802, "ns"),
            218,
            60.005,
            [
                pd.Timestamp("2019-07-10T18:13:11.134292801", tz="EST"),
                pd.Timestamp("2019-07-10T18:13:11.272011603", tz="EST"),
                pd.Timestamp("2019-07-10T18:14:11.139292768", tz="EST"),
                pd.Timestamp("2019-07-10T19:07:41.055190620", tz="EST"),
            ],
        ),
        (
            TEST_CST.XML_FILEPATH[1],
            1,
            10880,
            pd.Timedelta(221577885, "ns"),
            136,
            60.006,
            [
                pd.Timestamp("2020-02-04T18:01:38.714876000", tz="EST"),
                pd.Timestamp("2020-02-04T18:01:38.936453885", tz="EST"),
                pd.Timestamp("2020-02-04T18:02:38.720876000", tz="EST"),
                pd.Timestamp("2020-02-04T19:21:08.690862125", tz="EST"),
            ],
        ),
        (
            TEST_CST.XML_FILEPATH[1],
            2,
            10880,
            pd.Timedelta(221577885, "ns"),
            136,
            60.006,
            [
                pd.Timestamp("2020-02-04T18:01:38.788735295", tz="EST"),
                pd.Timestamp("2020-02-04T18:01:39.010313180", tz="EST"),
                pd.Timestamp("2020-02-04T18:02:38.794735375", tz="EST"),
                pd.Timestamp("2020-02-04T19:21:08.764721350", tz="EST"),
            ],
        ),
        (
            TEST_CST.XML_FILEPATH[1],
            3,
            10880,
            pd.Timedelta(221577885, "ns"),
            136,
            60.006,
            [
                pd.Timestamp("2020-02-04T18:01:38.862594590", tz="EST"),
                pd.Timestamp("2020-02-04T18:01:39.084172475", tz="EST"),
                pd.Timestamp("2020-02-04T18:02:38.868594750", tz="EST"),
                pd.Timestamp("2020-02-04T19:21:08.838580575", tz="EST"),
            ],
        ),
    ],
)
def test_get_frames_timestamps_from_xml(
    filepath,
    frame_index,
    frame_count,
    frame_time_delta,
    expected_time_frames,
    trial2_idx,
    time_between_trials,
):
    parser = MicroscopeParser(filepath=filepath)
    time_frames = parser.get_frames_timestamps_from_xml(frame_index)
    assert len(time_frames) == frame_count
    assert time_frames[0] == expected_time_frames[0]
    assert time_frames[1] == expected_time_frames[1]
    assert time_frames[trial2_idx] == expected_time_frames[2]
    assert time_frames[1] - time_frames[0] == frame_time_delta
    assert time_frames.iloc[-1] == expected_time_frames[-1]
    np.testing.assert_almost_equal(
        actual=(time_frames[trial2_idx] - time_frames[0]).total_seconds(),
        desired=time_between_trials,
        decimal=6,
    )


@pytest.mark.parametrize(
    "filepath, frame_index",
    [
        (TEST_CST.XML_FILEPATH[0], 0),
        (TEST_CST.XML_FILEPATH[0], 3),
        (TEST_CST.XML_FILEPATH[1], -1),
        (TEST_CST.XML_FILEPATH[1], 10),
    ],
)
def test_get_frames_timestamps_from_xml_indexerror(filepath, frame_index):
    parser = MicroscopeParser(filepath=filepath)
    with pytest.raises(IndexError):
        parser.get_frames_timestamps_from_xml(frame_index)


@pytest.mark.parametrize("frame_index", [5.2, None, 0.0003])
def test_get_frames_timestamps_from_xml_not_an_integer(frame_index):
    parser = MicroscopeParser(filepath=TEST_CST.XML_FILEPATH[0])
    with pytest.raises(IndexError):
        parser.get_frames_timestamps_from_xml(frame_index)


@pytest.mark.parametrize("version_nb", ["5.8.2", "1.3", "2.1.23"])
def test_ScopeUnsupportedVersion(version_nb):
    parser = MicroscopeParser(filepath=TEST_CST.XML_FILEPATH[2])
    with patch.object(MicroscopeParser, "get_version") as mock_version:
        mock_version.return_value = version_nb
        with pytest.raises(err.UnsupportedVersion):
            parser.get_frames_timestamps_from_xml(5)


@given(version_nb=st.from_regex(r"(?a)^\d{1,3}\.\d{1,3}\.\d{1,3}", fullmatch=True))
def test_scope_get_version_number(version_nb):
    file_content = f"""
<?xml version="1.0" encoding="utf-8"?>
<PVScan version="{version_nb}" date="7/10/2019 6:13:04 PM" notes="">
<SystemIDs SystemID="5A58-04F5-AB18-1690-65B3-44DE-A991-2BE6">
<SystemID SystemID="4796" Description="Brown University" />
</SystemIDs>
</PVScan>
"""
    XmlTestfile = Path("test.xml")
    with open(XmlTestfile, "w") as fid:
        fid.write(file_content.lstrip())

    parser = MicroscopeParser(filepath=XmlTestfile)
    parsed_version = parser.get_version()
    assert version.parse(parsed_version) == version.parse(version_nb)
    XmlTestfile.unlink()


def test_scope_get_version_number_UnsupportedVersion():
    file_content = """
<?xml version="1.0" encoding="utf-8"?>
<PVScan date="7/10/2019 6:13:04 PM" notes="">
<SystemIDs SystemID="5A58-04F5-AB18-1690-65B3-44DE-A991-2BE6">
<SystemID SystemID="4796" Description="Brown University" />
</SystemIDs>
</PVScan>
"""
    XmlTestfile = Path("test.xml")
    with open(XmlTestfile, "w") as fid:
        fid.write(file_content.lstrip())

    parser = MicroscopeParser(filepath=XmlTestfile)
    with pytest.raises(err.UnsupportedVersion):
        parser.get_version()
    XmlTestfile.unlink()


@pytest.mark.parametrize(
    "teensy_filepath, xml_filepath, row_indexes, row_values, expected_timestamps,"
    "experiment_version",
    [
        (
            TEST_CST.TEENSY_FILEPATHS[0],
            TEST_CST.XML_FILEPATH[0],
            [0, 29999, 30000, -1],
            [
                [8, 0.02286, np.nan, 1],
                [8, 0.020159999999999997, 1164, 1],
                [1, 0.02034, 326, 2],
                [5, 0.02112, 3185, 55],
            ],
            pd.Series(
                [
                    pd.Timestamp("2019-07-10T18:13:11.065433400", tz="EST"),
                    pd.Timestamp("2019-07-10T18:13:11.065433400", tz="EST")
                    + pd.Timedelta(seconds=29.999),
                    pd.Timestamp("2019-07-10T18:14:11.070433400", tz="EST"),
                    pd.Timestamp("2019-07-10T19:07:11.101433400", tz="EST")
                    + pd.Timedelta(seconds=29.999),
                ]
            ),
            CONST.EXPERIMENT_VERSIONS[1],
        ),
        (
            TEST_CST.TEENSY_FILEPATHS[2],
            TEST_CST.XML_FILEPATH[2],
            [0, 29999, 30000, -1],
            [
                [0.017939999999999998, 487],
                [0.01596, 686],
                [0.01566, 686],
                [0.014339999999999999, 1949],
            ],
            pd.Series(
                [
                    pd.Timestamp("2021/4/29T21:59:23.985212900", tz="EST"),
                    pd.Timestamp("2021/4/29T21:59:23.985212900", tz="EST")
                    + pd.Timedelta(seconds=29.999),
                    pd.Timestamp("2021/4/29T22:00:23.986212900", tz="EST"),
                    pd.Timestamp("2021/4/29T22:58:24.026212900", tz="EST")
                    + pd.Timedelta(seconds=30),
                ]
            ),
            CONST.EXPERIMENT_VERSIONS[2],
        ),
        (
            TEST_CST.TEENSY_FILEPATHS[5],
            TEST_CST.XML_FILEPATH[5],
            [0, 9999, 10000, 24999, 25000, -1],
            [
                [5, 0.008039999753236771, 938, 0, 1],
                [5, 0.025439999997615814, 1021, 0, 1],
                [5, 0.025319999083876610, 1021, 1, 1],
                [5, 0.025200000032782555, 1025, 0, 1],
                [2, 0.017640000209212303, np.nan, 0, 2],
                [4, 0.012659999541938305, 2118, 0, 90],
            ],
            pd.Series(
                [
                    pd.Timestamp("2022-05-31 18:26:31.836012200", tz="EST"),
                    pd.Timestamp("2022-05-31 18:26:31.836012200", tz="EST")
                    + pd.Timedelta(seconds=9.999),
                    pd.Timestamp("2022-05-31 18:26:31.836012200", tz="EST")
                    + pd.Timedelta(seconds=10),
                    pd.Timestamp("2022-05-31 18:26:31.836012200", tz="EST")
                    + pd.Timedelta(seconds=24.999),
                    pd.Timestamp("2022-05-31 18:27:11.836012200", tz="EST"),
                    pd.Timestamp("2022-05-31 19:25:51.880012200", tz="EST")
                    + pd.Timedelta(seconds=24.999),
                ]
            ),
            CONST.EXPERIMENT_VERSIONS[3],
        ),
    ],
)
def test_get_behavioral_data_from_teensy(
    teensy_filepath,
    row_values,
    row_indexes,
    expected_timestamps,
    xml_filepath,
    experiment_version,
):
    parser = TeensyParser(
        filepath=teensy_filepath, experiment_version=experiment_version
    )
    data = parser.get_behavioral_data_from_teensy(microscope_filepath=xml_filepath)
    for row_idx, row_val, timestamp in zip(
        row_indexes, row_values, expected_timestamps
    ):
        expected_values = np.array(row_val, dtype=np.float_)
        np.testing.assert_array_almost_equal(
            data.iloc[row_idx].values, expected_values, decimal=9
        )
        assert data.index[row_idx].isoformat() == timestamp.isoformat()


@pytest.mark.parametrize("version_nb", ["5.8.2", "1.3", "2.1.23"])
def test_TeensyUnsupportedVersion(version_nb):
    parser = TeensyParser(
        filepath=TEST_CST.TEENSY_FILEPATHS[0], experiment_version=version_nb
    )
    with pytest.raises(err.UnsupportedVersion):
        parser.get_behavioral_data_from_teensy(
            microscope_filepath=TEST_CST.XML_FILEPATH[0]
        )


@pytest.mark.parametrize(
    "time_array, trials_start, indexes, expected_timestamps",
    [
        (
            np.arange(1, 31),
            pd.date_range(
                start=datetime.datetime.fromtimestamp(0), periods=1, freq="s"
            ),
            [0],
            [pd.Timestamp(datetime.datetime.fromtimestamp(0))],
        ),
        (
            np.tile(np.arange(1, 31), 3),
            pd.date_range(
                start=datetime.datetime.fromtimestamp(0), periods=3, freq="s"
            ),
            [0, 29, 30, 59, 60, 89],
            [
                pd.Timestamp(datetime.datetime.fromtimestamp(0)),
                pd.Timestamp(datetime.datetime.fromtimestamp(0))
                + pd.Timedelta(milliseconds=29),
                pd.Timestamp(datetime.datetime.fromtimestamp(0))
                + pd.Timedelta(seconds=1),
                pd.Timestamp(datetime.datetime.fromtimestamp(0))
                + pd.Timedelta(seconds=1, milliseconds=29),
                pd.Timestamp(datetime.datetime.fromtimestamp(0))
                + pd.Timedelta(seconds=2),
                pd.Timestamp(datetime.datetime.fromtimestamp(0))
                + pd.Timedelta(seconds=2, milliseconds=29),
            ],
        ),
        (
            np.tile(np.arange(0, 30), 3),
            pd.date_range(
                start=datetime.datetime.fromtimestamp(0), periods=3, freq="s"
            ),
            [0, 29, 30, 59, 60, 89],
            [
                pd.Timestamp(datetime.datetime.fromtimestamp(0)),
                pd.Timestamp(datetime.datetime.fromtimestamp(0))
                + pd.Timedelta(milliseconds=29),
                pd.Timestamp(datetime.datetime.fromtimestamp(0))
                + pd.Timedelta(seconds=1),
                pd.Timestamp(datetime.datetime.fromtimestamp(0))
                + pd.Timedelta(seconds=1, milliseconds=29),
                pd.Timestamp(datetime.datetime.fromtimestamp(0))
                + pd.Timedelta(seconds=2),
                pd.Timestamp(datetime.datetime.fromtimestamp(0))
                + pd.Timedelta(seconds=2, milliseconds=29),
            ],
        ),
        (
            np.tile(np.arange(0, 10), 3),
            pd.date_range(
                start=datetime.datetime.fromtimestamp(0), periods=3, freq="min"
            ),
            [0, 9, 10, 19, 20, 29],
            [
                pd.Timestamp(datetime.datetime.fromtimestamp(0)),
                pd.Timestamp(datetime.datetime.fromtimestamp(0))
                + pd.Timedelta(milliseconds=9),
                pd.Timestamp(datetime.datetime.fromtimestamp(0))
                + pd.Timedelta(minutes=1),
                pd.Timestamp(datetime.datetime.fromtimestamp(0))
                + pd.Timedelta(minutes=1, milliseconds=9),
                pd.Timestamp(datetime.datetime.fromtimestamp(0))
                + pd.Timedelta(minutes=2),
                pd.Timestamp(datetime.datetime.fromtimestamp(0))
                + pd.Timedelta(minutes=2, milliseconds=9),
            ],
        ),
        (
            np.concatenate(
                [
                    np.arange(0, 10),
                    np.arange(1, 15),
                    np.arange(0, 1000),
                    np.arange(2, 10),
                ]
            ),
            pd.date_range(
                start=datetime.datetime.fromtimestamp(0), periods=4, freq="s"
            ),
            [0, 9, 10, 23, 24, 1023, 1024, -1],
            [
                pd.Timestamp(datetime.datetime.fromtimestamp(0)),
                pd.Timestamp(datetime.datetime.fromtimestamp(0))
                + pd.Timedelta(milliseconds=9),
                pd.Timestamp(datetime.datetime.fromtimestamp(0))
                + pd.Timedelta(seconds=1),
                pd.Timestamp(datetime.datetime.fromtimestamp(0))
                + pd.Timedelta(seconds=1, milliseconds=13),
                pd.Timestamp(datetime.datetime.fromtimestamp(0))
                + pd.Timedelta(seconds=2),
                pd.Timestamp(datetime.datetime.fromtimestamp(0))
                + pd.Timedelta(seconds=2, milliseconds=999),
                pd.Timestamp(datetime.datetime.fromtimestamp(0))
                + pd.Timedelta(seconds=3),
                pd.Timestamp(datetime.datetime.fromtimestamp(0))
                + pd.Timedelta(seconds=3, milliseconds=7),
            ],
        ),
    ],
)
def test_create_aligned_timestamps(
    time_array, expected_timestamps, trials_start, indexes
):
    timestamps = create_aligned_timestamps(
        time_array=time_array, external_trials_start=trials_start
    )
    for idx, index in enumerate(indexes):
        assert (
            timestamps.iloc[index].isoformat() == expected_timestamps[idx].isoformat()
        )


@pytest.mark.parametrize(
    "time_array, trials_start, data_pts_len, trial_nb",
    [
        (
            np.concatenate(
                [
                    np.arange(0, 10),
                    np.arange(1, 15),
                    np.arange(0, 1020),
                    np.arange(2, 10),
                ]
            ),
            pd.date_range(
                start=datetime.datetime.fromtimestamp(0), periods=4, freq="s"
            ),
            20,
            3,
        ),
        (
            np.concatenate(
                [
                    np.arange(0, 10),
                    np.arange(1, 15),
                    np.arange(0, 50),
                    np.arange(0, 1500),
                    np.arange(5, 50),
                ]
            ),
            pd.date_range(
                start=datetime.datetime.fromtimestamp(0), periods=5, freq="s"
            ),
            500,
            4,
        ),
    ],
)
def test_create_aligned_timestamps_warning(
    time_array, trials_start, data_pts_len, trial_nb
):
    with pytest.warns(
        UserWarning, match=f"Dropping {data_pts_len} data points from trial {trial_nb}"
    ):
        create_aligned_timestamps(
            time_array=time_array, external_trials_start=trials_start
        )


@pytest.mark.parametrize(
    "time_array, trials_start, assert_message",
    [
        (
            np.concatenate(
                [
                    np.arange(0, 10),
                    np.arange(1, 15),
                    np.arange(2, 15),
                    np.arange(1, 28),
                ]
            ),
            pd.date_range(
                start=datetime.datetime.fromtimestamp(0), periods=2, freq="s"
            ),
            "Number of trials in time does not match length of the start times",
        ),
        (
            np.concatenate(
                [
                    np.arange(0, 10),
                    np.arange(1, 15),
                    np.arange(2, 20),
                ]
            ),
            pd.date_range(
                start=datetime.datetime.fromtimestamp(0), periods=5, freq="s"
            ),
            "Number of trials in time does not match length of the start times",
        ),
        (
            np.concatenate(
                [
                    np.array([0, 1, 2, 2, 2, 4, 4, 5]),
                    np.array([2, 3, 4, 5, 6, 7, 7, 7, 8]),
                    np.arange(1, 20),
                ]
            ),
            pd.date_range(
                start=datetime.datetime.fromtimestamp(0), periods=3, freq="s"
            ),
            "Consecutive time between 2 samples is not increasing",
        ),
        (
            np.concatenate(
                [
                    np.arange(2, 40),
                    np.arange(1, 20),
                    np.array([19, 19, 20, 21]),
                    np.arange(21, 30),
                ]
            ),
            pd.date_range(
                start=datetime.datetime.fromtimestamp(0), periods=2, freq="s"
            ),
            "Consecutive time between 2 samples is not increasing",
        ),
        (
            np.concatenate(
                [
                    np.arange(1, 10),
                    np.array([2, 3, 4, 5, 6, 7, 10, 11.5, 12, 13, 14, 15]),
                    np.arange(1, 20),
                ]
            ),
            pd.date_range(
                start=datetime.datetime.fromtimestamp(0), periods=3, freq="s"
            ),
            "Consecutive time between 2 samples is not homogeneous",
        ),
        (
            np.concatenate(
                [
                    np.arange(1, 10),
                    np.arange(0, 25, 2),
                    np.arange(2, 30),
                    np.arange(0, 15, 3),
                ]
            ),
            pd.date_range(
                start=datetime.datetime.fromtimestamp(0), periods=4, freq="s"
            ),
            "Consecutive time between 2 samples is not homogeneous",
        ),
        (  # the lower ceiling here, if go below -17 cannot distinguish
            np.concatenate(
                [
                    np.arange(1, 10, 1 + 1e-16),
                    np.arange(0, 30, 1 + 2e-16),
                ]
            ),
            pd.date_range(
                start=datetime.datetime.fromtimestamp(0), periods=2, freq="s"
            ),
            "Consecutive time between 2 samples is not homogeneous",
        ),
        (  # need checking for very small random noise in sampling
            np.concatenate(
                [
                    np.arange(1, 30) + 1e-15 * (np.random.rand(29) - 0.5),
                    np.arange(0, 25),
                ]
            ),
            pd.date_range(
                start=datetime.datetime.fromtimestamp(0), periods=2, freq="s"
            ),
            "Consecutive time between 2 samples is not homogeneous",
        ),
    ],
)
def test_create_aligned_timestamps_asserts(time_array, trials_start, assert_message):
    with pytest.raises(AssertionError, match=assert_message):
        create_aligned_timestamps(
            time_array=time_array, external_trials_start=trials_start
        )


@pytest.mark.parametrize(
    "filepath, expected_encoding",
    [(TEST_CST.TEENSY_FILEPATHS[0], "ascii"), (TEST_CST.TEENSY_FILEPATHS[1], "UTF-16")],
)
def test_detect_encoding(filepath, expected_encoding):
    parser = TeensyParser(
        filepath=filepath, experiment_version=CONST.EXPERIMENT_VERSIONS[1]
    )
    enc = parser.detect_encoding()
    assert enc == expected_encoding


@pytest.mark.parametrize(
    "file_content, row_values, experiment_version",
    [
        (
            """
Hello
Done with setup
a1>
oData,30716,1,370,3779,0
,About to pick
9
4
3
1
5
2
8
7
10
6
----
new odor # 9
will trigger
Trig microscope
Open air flow
0
oData,1,9,369,3779,1
,oData,2,9,370,3779,1
,oData,3,9,372,3779,1
,oData,4,9,372,3779,1
,oData,5,9,370,3889,1
,oData,6,9,368,3889,1
,oData,7,9,369,3889,1
,oData,8,9,367,3889,1
,oData,9,9,371,3777,1
,oData,10,9,374,3777,1
,oData,11,9,371,3777,1
,oData,12,9,371,3777,1
,oData,13,9,372,3776,1
,oData,14,9,370,3776,1
,oData,15,9,368,3776,1
,oData,16,9,371,3776,1
,oData,17,9,372,3776,1
,oData,18,9,370,3776,1
,oData,19,9,370,3776,1
,oData,20,9,370,3776,1
,oData,21,9,367,3777,1
,oData,22,9,367,3777,1
,oData,23,9,370,3777,1
,oData,24,9,370,3777,1
,oData,25,9,367,3762,1
,oData,26,9,370,3762,1
,oData,27,9,369,3762,1
,oData,28,9,367,3762,1
,oData,29,9,370,3775,1
,Finished odor;
waiting for the next odor; pausing for 30000
60000
oData,0,4,366,3772,1
""",
            [
                (
                    0,
                    [9, 0.02214, 3779, 1],
                ),
                (28, [9, 0.022199999999999998, 3775, 1]),
            ],
            CONST.EXPERIMENT_VERSIONS[1],
        ),
        (
            """
Hello
Done with setup
a1>
33901,1,3,0,0,2201
About to pick
5
1
3
6
4
2
7
8
9
10
----
new odor # 5
will trigger
Trig microscope
Open air flow
0
1,5,0,0,1,1053
2,5,2,1229,1,985
3,5,2,1229,1,1086
4,5,2,1229,1,1177
5,5,3,1229,1,895
6,5,5,516,1,936
7,5,3,516,1,920
8,5,2,516,1,714
9,5,3,516,1,785
10,5,6,516,1,860
11,5,1,516,1,996
12,5,5,516,1,676
13,5,1,516,1,751
14,5,3,516,1,983
15,5,7,516,1,861
16,5,8,516,1,786
17,5,7,516,1,1142
18,5,3,516,1,912
19,5,3,516,1,797
20,5,3,516,1,707
21,5,5,516,1,840
22,5,6,516,1,879
23,5,4,516,1,827
24,5,2,516,1,666
25,5,3,516,1,828
26,5,1,516,1,997
27,5,4,516,1,954
28,5,5,516,1,833
29,5,5,516,1,644
Finished odor;
waiting for the next odor; pausing for 10000
starting group # 11
32000
""",
            [
                (
                    0,
                    [5, 0, 1],
                ),
                (28, [5, 516, 1]),
            ],
            CONST.EXPERIMENT_VERSIONS[0],
        ),
        (
            """
Hello
Done with setup
a1>
oData,20115,1,2947,991,0,0
About to pick
25
1
12
10
11
9
24
26
2
27
----
new odor # 25
will trigger
Trig microscope
Open air flow
0
oData,1,25,2166,991,1,0
oData,2,25,1670,991,1,0
oData,3,25,1386,1960,1,0
oData,4,25,1355,1960,1,0
oData,5,25,1197,1960,1,0
oData,6,25,1166,1960,1,0
oData,7,25,1100,982,1,0
oData,8,25,1112,982,1,0
oData,9,25,1179,982,1,0
oData,10,25,1138,982,1,0
oData,11,25,854,982,1,0
oData,12,25,637,982,1,0
oData,13,25,573,982,1,0
oData,14,25,658,982,1,0
oData,15,25,694,982,1,0
oData,16,25,631,982,1,0
oData,17,25,633,982,1,0
oData,18,25,622,982,1,0
oData,19,25,654,982,1,0
oData,20,25,834,982,1,0
oData,21,25,929,982,1,0
oData,22,25,1010,982,1,0
oData,23,25,1069,982,1,0
oData,24,25,905,982,1,0
oData,25,25,784,982,1,0
oData,26,25,933,982,1,0
oData,27,25,821,982,1,0
oData,28,25,745,982,1,0
oData,29,25,541,982,1,0
Finished odor;
waiting for the next odor; pausing for 30000
60000
oData,0,1,2831,1727,1,0
""",
            [
                (
                    0,
                    [25, 991, 1],
                ),
                (28, [25, 982, 1]),
            ],
            CONST.EXPERIMENT_VERSIONS[4],
        ),
    ],
)
@patch("calimag.parsers.create_aligned_timestamps")
@patch("calimag.parsers.TeensyParser.detect_encoding")
@patch("calimag.parsers.MicroscopeParser.get_version")
@patch("calimag.parsers.MicroscopeParser.get_trials_timestamps_from_xml")
@patch("xml.etree.ElementTree.parse")
def test_get_behavioral_data_from_teensy_with_comma(
    mocked_XML,
    mocked_trials_timestamps,
    mocked_version,
    mocked_encoding,
    mocked_timestamps,
    file_content,
    row_values,
    experiment_version,
):
    parser = TeensyParser(
        experiment_version=experiment_version, filepath=TEST_CST.TEENSY_FILEPATHS[0]
    )

    # Prepare all mocking objects
    # Most of them are needed because of the `mock_open()` helper
    # which gets triggered whenever a file gets opened
    parsed_xml = ET.parse(TEST_CST.XML_FILEPATH[0])
    mocked_XML.return_value = parsed_xml
    mocked_trials_timestamps.return_value = pd.date_range(
        start=datetime.datetime.fromtimestamp(0), periods=2, freq="min"
    )
    mocked_version.return_value = "5.4.64.700"
    mocked_encoding.return_value = "utf-8"
    mocked_timestamps.return_value = [
        datetime.datetime.fromtimestamp(0) + pd.Timedelta(microseconds=idx)
        for idx in range(29)
    ]

    with patch("builtins.open", mock_open(read_data=file_content)):
        data = parser.get_behavioral_data_from_teensy(
            microscope_filepath=TEST_CST.XML_FILEPATH[0]
        )
        for row_value in row_values:
            expected_values = np.array(row_value[1], dtype=np.float_)
            np.testing.assert_array_almost_equal(
                data.iloc[row_value[0]].values,
                expected_values,
                decimal=9,
            )


@pytest.mark.parametrize(
    "start_time, timestamps, expected_time",
    [
        (
            datetime.datetime(
                year=2020, month=12, day=18, hour=19, minute=1, second=30
            ),
            [
                datetime.datetime(
                    year=2020, month=12, day=18, hour=19, minute=3, second=30
                )
                + datetime.timedelta(seconds=item)
                for item in range(0, 10)
            ],
            np.arange(120, 120 + 10),
        ),
        (
            np.datetime64("2020-12-18T19:01:30"),
            np.arange(
                "2020-12-18T19:03:30", "2020-12-18T19:03:40", dtype="datetime64[s]"
            ),
            np.arange(120, 120 + 10),
        ),
        (
            datetime.datetime(
                year=2020,
                month=12,
                day=18,
                hour=19,
                minute=1,
                second=30,
                microsecond=10,
            ),
            [
                datetime.datetime(
                    year=2020,
                    month=12,
                    day=18,
                    hour=19,
                    minute=3,
                    second=30,
                    microsecond=10,
                )
                + datetime.timedelta(milliseconds=item)
                for item in range(0, 10)
            ],
            np.linspace(120, 120.009, 10),
        ),
        (
            datetime.datetime(
                year=2020,
                month=12,
                day=18,
                hour=19,
                minute=1,
                second=30,
                microsecond=10,
            ),
            [
                datetime.datetime(
                    year=2020,
                    month=12,
                    day=18,
                    hour=19,
                    minute=3,
                    second=30,
                    microsecond=10,
                )
                + datetime.timedelta(microseconds=item)
                for item in range(0, 10)
            ],
            np.linspace(120, 120.000009, 10),
        ),
        (
            np.datetime64("2020-02-04T18:00:52.6698760"),
            np.arange(
                "2020-02-04T18:00:54.6698760",
                "2020-02-04T18:00:54.669876010",
                dtype="datetime64[ns]",
            ),
            np.linspace(2, 2.000000009, 10),
        ),
        (
            pd.Timestamp("2013-12-12 00:00:00", tz="CET"),
            pd.Series(
                pd.date_range("2013-12-12 00:01:00", freq="ns", periods=10, tz="CET")
            ),
            np.linspace(60, 60.000000009, 10),
        ),
    ],
)
def test_get_timedelta_from_start_time(start_time, timestamps, expected_time):
    timestamps_st = get_timedelta_from_start_time(
        start_time=start_time, timestamps=timestamps
    )
    np.testing.assert_array_almost_equal(timestamps_st, expected_time, decimal=9)


def test_TimestampsStartBeforeFileStartTime():
    start_time = np.datetime64("2020-12-23T14:48:30")
    timestamps = np.arange(
        "2020-12-18T19:03:30", "2020-12-18T19:03:40", dtype="datetime64[s]"
    )
    with pytest.raises(err.TimestampsStartBeforeFileStartTime):
        get_timedelta_from_start_time(start_time=start_time, timestamps=timestamps)


@pytest.mark.parametrize(
    "xml_filepath, expected_session_start_time",
    [
        (TEST_CST.XML_FILEPATH[0], "2019-07-10T18:13:11.065433400-05:00"),
        (TEST_CST.XML_FILEPATH[1], "2020-02-04T18:01:38.714876-05:00"),
    ],
)
def test_get_session_start_time(xml_filepath, expected_session_start_time):
    parser = MicroscopeParser(filepath=xml_filepath)
    golbal_ref_time = parser.get_session_start_time()
    assert golbal_ref_time.isoformat() == expected_session_start_time


def test_get_session_start_time_SessionStartTimeError():
    file_content = """
<?xml version="1.0" encoding="utf-8"?>
<PVScan version="5.4.64.700" notes="">
  <SystemIDs SystemID="5A58-04F5-AB18-1690-65B3-44DE-A991-2BE6">
    <SystemID SystemID="4796" Description="Brown University" />
  </SystemIDs>
</PVScan>
"""
    XmlTestfile = Path("test.xml")
    with open(XmlTestfile, "w") as fid:
        fid.write(file_content.lstrip())

    with pytest.raises(err.SessionStartTimeError):
        parser = MicroscopeParser(filepath=XmlTestfile)
        parser.get_session_start_time()

    XmlTestfile.unlink()


@pytest.mark.parametrize(
    "xml_sequence",
    [
        """
  <Sequence type="TSeries ZSeries Element" bidirectionalZ="False">
  </Sequence>
        """,
        """
  <Sequence type="TSeries ZSeries Element" cycle="1" time="18:00:52.6698760">
    <PVStateShard />
    <Frame relativeTime="0" parameterSet="CurrentSettings">
    </Frame>
  </Sequence>
        """,
    ],
)
def test_get_session_start_time_XmlElementNotFound(xml_sequence):
    file_content = f"""
<?xml version="1.0" encoding="utf-8"?>
<PVScan version="5.4.64.700" date="2/4/2020 6:00:52 PM" notes="">
  <SystemIDs SystemID="5A58-04F5-AB18-1690-65B3-44DE-A991-2BE6">
    <SystemID SystemID="4796" Description="Brown University" />
  </SystemIDs>
{xml_sequence}
</PVScan>
"""
    XmlTestfile = Path("test.xml")
    with open(XmlTestfile, "w") as fid:
        fid.write(file_content.lstrip())

    with pytest.raises(err.XmlElementNotFound):
        parser = MicroscopeParser(filepath=XmlTestfile)
        parser.get_session_start_time()

    XmlTestfile.unlink()


@pytest.mark.parametrize(
    "xml_filepath, expected_trials_timestamps",
    [
        (
            TEST_CST.XML_FILEPATH[0],
            [
                pd.Timestamp("2019-07-10T18:13:11.065433400", tz="EST"),
                pd.Timestamp("2019-07-10T18:14:11.070433400", tz="EST"),
                pd.Timestamp("2019-07-10T19:07:11.101433400", tz="EST"),
            ],
        ),
        (
            TEST_CST.XML_FILEPATH[1],
            [
                pd.Timestamp("2020-02-04T18:01:38.714876000", tz="EST"),
                pd.Timestamp("2020-02-04T18:02:38.720876000", tz="EST"),
                pd.Timestamp("2020-02-04T19:20:38.777876000", tz="EST"),
            ],
        ),
    ],
)
def test_get_trials_timestamps_from_xml(xml_filepath, expected_trials_timestamps):
    parser = MicroscopeParser(filepath=xml_filepath)
    trials_timestamps = parser.get_trials_timestamps_from_xml()
    indexes = [0, 1, -1]
    for idx in indexes:
        assert (
            trials_timestamps.iloc[idx].isoformat()
            == expected_trials_timestamps[idx].isoformat()
        )


@given(version_nb=st.from_regex(r"(?a)(\d\.)+\d+", fullmatch=True))
# (?a) To avoid generating Unicode characters
def test_get_trials_timestamps_from_xml_UnsupportedVersion(version_nb):
    file_content = f"""
<?xml version="1.0" encoding="utf-8"?>
<PVScan version="{version_nb}" date="7/10/2019 6:13:04 PM" notes="">
<SystemIDs SystemID="5A58-04F5-AB18-1690-65B3-44DE-A991-2BE6">
<SystemID SystemID="4796" Description="Brown University" />
</SystemIDs>
</PVScan>
"""
    XmlTestfile = Path("test.xml")
    with open(XmlTestfile, "w") as fid:
        fid.write(file_content.lstrip())

    parser = MicroscopeParser(filepath=XmlTestfile)
    with pytest.raises(err.UnsupportedVersion):
        parser.get_trials_timestamps_from_xml()

    XmlTestfile.unlink()


@pytest.mark.parametrize(
    "xml_filepath, expected_nchannels",
    [
        (TEST_CST.XML_FILEPATH[0], 1),
        (TEST_CST.XML_FILEPATH[1], 1),
        (TEST_CST.XML_FILEPATH[2], 1),
        (TEST_CST.XML_FILEPATH[3], 2),
    ],
)
def test_get_number_of_channels(xml_filepath, expected_nchannels):
    parser = MicroscopeParser(filepath=xml_filepath)
    nchannels = parser.get_number_of_channels()
    assert nchannels == expected_nchannels


@pytest.mark.parametrize(
    "xml_filepath, expected_excitation_lambda",
    [
        (TEST_CST.XML_FILEPATH[0], 920),
        (TEST_CST.XML_FILEPATH[1], 920),
        (TEST_CST.XML_FILEPATH[2], 960),
        (TEST_CST.XML_FILEPATH[3], 960),
    ],
)
def test_get_excitation_lambda(xml_filepath, expected_excitation_lambda):
    parser = MicroscopeParser(filepath=xml_filepath)
    excitation_lambda = parser.get_excitation_lambda()
    assert isinstance(excitation_lambda, float)
    assert excitation_lambda == expected_excitation_lambda
