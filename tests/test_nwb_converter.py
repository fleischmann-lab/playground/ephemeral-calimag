from pathlib import Path
from unittest.mock import patch

import numpy as np
import pandas as pd
import pytest
import test_constants as TEST_CST
import toml
from pynwb import NWBHDF5IO, NWBFile, TimeSeries
from pynwb.epoch import TimeIntervals

from calimag.config import GetConfig
from calimag.nwb_converter import NwbConverter


@pytest.mark.parametrize(
    "config_filepath,teensy_filepath, microscope_filepath",
    [
        (
            TEST_CST.CONFIG_FILEPATH[0],
            TEST_CST.TEENSY_FILEPATHS[4],
            TEST_CST.XML_FILEPATH[4],
        ),
        (
            TEST_CST.CONFIG_FILEPATH[2],
            TEST_CST.TEENSY_FILEPATHS[5],
            TEST_CST.XML_FILEPATH[5],
        ),
    ],
)
def test_ProcessTimeSeries(config_filepath, teensy_filepath, microscope_filepath):
    conv = NwbConverter(config_filepath=config_filepath)
    data_ts = conv.ProcessTimeSeries(
        teensy_filepath=teensy_filepath, microscope_filepath=microscope_filepath
    )
    for name in data_ts.acquisition:
        container = data_ts.acquisition.get(name)
        assert isinstance(container, TimeSeries)
        if name == "flow":
            assert container.unit == "m3/sec"
        elif name == "wheel":
            assert container.unit == "rad"
    for name in data_ts.stimulus:
        container = data_ts.stimulus.get(name)
        assert isinstance(container, TimeSeries)
        if "odor" in name:
            assert container.unit == "-"
    assert isinstance(data_ts.trials, TimeIntervals)
    GetConfig.cache_clear()


# def test_ProcessImagingData():
#     conv = NwbConverter(
#         session_description=SESSION_DESCRIPTION,
#         microscope_filepath=XML_FILEPATH[4],
#         teensy_filepath=TEENSY_FILEPATHS[4],
#         suite2pNpyPath=str(SUITE2PNPYPATH),
#         TwoPhotonSeriesUrlOrPath=TWOPHOTONSERIES,
#     )
#     imgfile = conv.ProcessImagingData(
#         xml_filepath=XML_FILEPATH[4],
#         nwb2p_filepath=NWB2P,
#         TwoPhotonSeriesUrlOrPath=TWOPHOTONSERIES,
#     )
#     assert imgfile


# def test_Convert2NWB():
#     nwb_test_path = DATA_PATH.joinpath(
#         "test.nwb",
#     )
#     if nwb_test_path.exists():
#         nwb_test_path.unlink()

#     conv = NwbConverter(
#         session_description=SESSION_DESCRIPTION,
#         microscope_filepath=str(XML_FILEPATH[4]),
#         teensy_filepath=str(TEENSY_FILEPATHS[4]),
#         segmentation_fp=str(NWB2P),
#         TwoPhotonSeriesUrlOrPath=TWOPHOTONSERIES,
#     )
#     nwb = conv.Convert2NWB(nwb_output_path=str(nwb_test_path))
#     assert isinstance(nwb, NWBFile)

#     # Rename segmentation file to check that there are no links between files
#     NWB2P_backup = NWB2P
#     NWB2P.rename("dummy.test")

#     with NWBHDF5IO(str(nwb_test_path), "r") as io:
#         read_nwbfile = io.read()
#         assert read_nwbfile.acquisition
#         assert read_nwbfile.stimulus
#         assert read_nwbfile.trials
#         assert read_nwbfile.processing
#         assert read_nwbfile.processing["ophys"].data_interfaces["Deconvolved"]
#         assert read_nwbfile.processing["ophys"].data_interfaces["Fluorescence"]
#         assert read_nwbfile.processing["ophys"].data_interfaces["Neuropil"]
#     # Revert and clean
#     NWB2P.rename(NWB2P_backup)
#     nwb_test_path.unlink()


def test_Convert2NWBfromNpy_main():
    config = toml.load(TEST_CST.CONFIG_FILEPATH[0])
    nwb_test_path = Path(config.get("DataInput").get("nwb_output_filepath_mandatory"))
    if nwb_test_path.exists():
        nwb_test_path.unlink()

    conv = NwbConverter(config_filepath=TEST_CST.CONFIG_FILEPATH[0])
    nwb = conv.Convert2NWB()
    assert isinstance(nwb, NWBFile)

    with NWBHDF5IO(str(nwb_test_path), "r") as io:
        read_nwbfile = io.read()
        assert read_nwbfile.acquisition
        assert read_nwbfile.stimulus
        assert read_nwbfile.trials
        assert read_nwbfile.subject
        assert read_nwbfile.processing
        ophys = read_nwbfile.processing["ophys"]
        assert ophys.data_interfaces["Deconvolved"]
        Fluorescence = ophys.data_interfaces["Fluorescence"]
        assert Fluorescence
        assert ophys.data_interfaces["Neuropil"]
        assert Fluorescence.roi_response_series["Plane_1"].data[:].shape == (10880, 142)
        assert Fluorescence.roi_response_series["Plane_2"].data[:].shape == (10880, 74)
        assert Fluorescence.roi_response_series["Plane_3"].data[:].shape == (10880, 99)
        assert Fluorescence.roi_response_series["Plane_1"].timestamps.shape == (10880,)
        assert Fluorescence.roi_response_series["Plane_2"].timestamps.shape == (10880,)
        assert Fluorescence.roi_response_series["Plane_3"].timestamps.shape == (10880,)
        np.testing.assert_array_equal(
            Fluorescence.roi_response_series["Plane_1"].rois[:].index.to_numpy(),
            np.arange(0, 142),
        )
        np.testing.assert_array_equal(
            Fluorescence.roi_response_series["Plane_2"].rois[:].index.to_numpy(),
            np.arange(142, 216),
        )
        np.testing.assert_array_equal(
            Fluorescence.roi_response_series["Plane_3"].rois[:].index.to_numpy(),
            np.arange(216, 315),
        )

        # Check that the timestamps from the neural + behavioral data
        # start at the same time
        global_ref_time = pd.Timestamp(read_nwbfile.timestamps_reference_time)
        assert global_ref_time.isoformat() == "2020-02-04T18:01:38.714876-05:00"
        roi_response_series = ophys.data_interfaces["Fluorescence"].roi_response_series
        flow = read_nwbfile.acquisition["flow"]
        assert (
            global_ref_time
            + pd.Timedelta(seconds=roi_response_series["Plane_1"].timestamps[0])
        ).isoformat() == "2020-02-04T18:01:38.714876-05:00"
        assert (
            global_ref_time + pd.Timedelta(seconds=flow.timestamps[0])
        ).isoformat() == "2020-02-04T18:01:38.714876-05:00"

        trial_start = read_nwbfile.trials["start_time"][:]
        trial_end = read_nwbfile.trials["stop_time"][:]
        trial_start_expected = np.array([0.0, 60.006, 120.003, 180.005, 240.005])
        trial_end_expected = np.array([29.999, 90.005, 150.002, 210.004, 270.004])
        np.testing.assert_array_equal(
            trial_start[0:5],
            trial_start_expected,
        )
        np.testing.assert_array_equal(
            trial_end[0:5],
            trial_end_expected,
        )

    nwb_test_path.unlink()
    GetConfig.cache_clear()


def test_NwbConverter_no_subject():
    with patch("toml.load") as mocked_config:
        mocked_config.return_value = {
            "DataInput": {
                "microscope_filepath_mandatory": "./data/Npy/"
                "TSeries-02042020-1448-001.xml",
                "teensy_filepath_mandatory": "./data/Npy/serialdata_m8.txt",
                "suite2pNpy_directory_mandatory": "./data/Npy",
                "nwb_output_filepath_mandatory": "./data/Npy/test.nwb",
            },
            "NWBFile": {
                "session_description_mandatory": "My awesome session",
                "lab": "Fleischmann Lab",
                "institution": "Brown University",
                "timezone": "EST",
            },
            "Imaging": {
                "Device": {"Type_mandatory": "Microscope"},
                "PlaneDescription_mandatory": "My awesome imaging plane",
            },
        }
        conv = NwbConverter(config_filepath=TEST_CST.CONFIG_FILEPATH[0])
        assert conv.nwbfile
        assert conv.config.get("Subject") is None
        GetConfig.cache_clear()


def test_Convert2NWBfromNpy_chan2():
    config = toml.load(TEST_CST.CONFIG_FILEPATH[1])
    nwb_test_path = Path(config.get("DataInput").get("nwb_output_filepath_mandatory"))
    if nwb_test_path.exists():
        nwb_test_path.unlink()

    conv = NwbConverter(config_filepath=TEST_CST.CONFIG_FILEPATH[1])
    nwb = conv.Convert2NWB()
    assert isinstance(nwb, NWBFile)

    with NWBHDF5IO(str(nwb_test_path), "r") as io:
        read_nwbfile = io.read()
        assert read_nwbfile.acquisition
        assert read_nwbfile.stimulus
        assert read_nwbfile.processing
        ophys = read_nwbfile.processing["ophys"]
        assert ophys.data_interfaces["Deconvolved"]
        Fluorescence = ophys.data_interfaces["Fluorescence"]
        assert Fluorescence
        np.testing.assert_array_equal(
            Fluorescence.roi_response_series["Plane_1"].rois[:].index.to_numpy(),
            np.arange(0, 207),
        )
        np.testing.assert_array_equal(
            Fluorescence.roi_response_series["Plane_2"].rois[:].index.to_numpy(),
            np.arange(207, 530),
        )
        np.testing.assert_array_equal(
            Fluorescence.roi_response_series["Plane_3"].rois[:].index.to_numpy(),
            np.arange(530, 669),
        )
        Fluorescence_chan2 = ophys.data_interfaces["Fluorescence_chan2"]
        assert Fluorescence_chan2
        np.testing.assert_array_equal(
            Fluorescence_chan2.roi_response_series["Plane_1"].rois[:].index.to_numpy(),
            np.arange(0, 207),
        )
        np.testing.assert_array_equal(
            Fluorescence_chan2.roi_response_series["Plane_2"].rois[:].index.to_numpy(),
            np.arange(207, 530),
        )
        np.testing.assert_array_equal(
            Fluorescence_chan2.roi_response_series["Plane_3"].rois[:].index.to_numpy(),
            np.arange(530, 669),
        )
        assert ophys.data_interfaces["Neuropil"]
        assert ophys.data_interfaces["Neuropil_chan2"]
        global_ref_time = pd.Timestamp(read_nwbfile.timestamps_reference_time)
        assert global_ref_time.isoformat() == "2021-05-11T14:57:25.952809-05:00"

    nwb_test_path.unlink()
    GetConfig.cache_clear()
