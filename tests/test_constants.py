from pathlib import Path

DATA_PATH = Path("data")
TEST_PATH = Path("tests")
SUITE2PNPYPATH = DATA_PATH.joinpath("Npy")

TEENSY_FILEPATHS = [
    DATA_PATH.joinpath("serialdata - 20190710_M002.txt"),
    DATA_PATH.joinpath("serial_mouse#7.txt"),
    DATA_PATH.joinpath("spontaneous-activity", "serial data.txt"),
    DATA_PATH.joinpath("20210511_Mouse#513_Teensy_serial_data.csv"),
    SUITE2PNPYPATH.joinpath("serialdata_m8.txt"),
    DATA_PATH.joinpath("v2022_05-2p_imaging_head_fixed-Max", "20220531_ME02.txt"),
]
XML_FILEPATH = [
    DATA_PATH.joinpath(
        "metadata - TSeries-07102019-1600-023.xml",
    ),
    DATA_PATH.joinpath(
        "Npy",
        "TSeries-02042020-1448-001.xml",
    ),
    DATA_PATH.joinpath("spontaneous-activity", "TSeries-04292021-2139-010.xml"),
    DATA_PATH.joinpath("Raw_Data_Example", "TSeries-05112021-1455-001.xml"),
    SUITE2PNPYPATH.joinpath(
        "TSeries-02042020-1448-001.xml",
    ),
    DATA_PATH.joinpath(
        "v2022_05-2p_imaging_head_fixed-Max", "TSeries-05312022-1348-020.xml"
    ),
]
CONFIG_FILEPATH = [
    TEST_PATH.joinpath("user.config.toml"),
    DATA_PATH.joinpath("Raw_Data_Example", "config.toml"),
    DATA_PATH.joinpath("v2022_05-2p_imaging_head_fixed-Max", "config.toml"),
]
NWB2P = DATA_PATH.joinpath("ophys.nwb")
