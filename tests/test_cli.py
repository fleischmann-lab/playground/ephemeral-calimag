import shutil
from pathlib import Path
from unittest.mock import patch

import pytest
import test_constants as TEST_CST
from click.testing import CliRunner
from filelock import FileLock
from pynwb import NWBHDF5IO

import calimag as calimag
from calimag.cli import cli
from calimag.config import GetConfig


@pytest.mark.parametrize(
    "empty_fields_input, continue_input, empty_fields",
    [
        ("doesn't matter", "y", False),
        ("n", "doesn't matter", False),
        ("garbage", "garbage", False),
        ("Y", "Y", False),
        ("N", "doesn't matter", False),
        ("y", "n", True),
        ("y", "N", True),
        ("y", "y", True),
        ("n", "doesn't matter", True),
    ],
)
def test_cli(empty_fields_input, continue_input, empty_fields):
    if empty_fields:
        config_filepath = TEST_CST.CONFIG_FILEPATH[1]
    else:
        config_filepath = TEST_CST.CONFIG_FILEPATH[0]
    config = GetConfig(config_filepath)
    nwb_test_path = config.get("DataInput").get("nwb_output_filepath_mandatory")
    lock_path = Path(str(nwb_test_path) + ".lock")
    lock = FileLock(lock_path, timeout=1)
    lock.acquire(timeout=30)
    if nwb_test_path.exists():
        nwb_test_path.unlink()

    # Mock the returned NWB file to make testing faster
    if continue_input.lower() == "y":
        mocked_nwb_path = Path("data").joinpath("ophys.nwb")
        shutil.copy(mocked_nwb_path, nwb_test_path)

    def return_nwb_if_y():
        if continue_input.lower() == "y":
            with NWBHDF5IO(str(nwb_test_path), "r") as io:
                mocked_nwb = io.read()
            return mocked_nwb
        else:
            return False

    if empty_fields:
        input_mock = f"{empty_fields_input}\n{continue_input}\n"
    else:
        input_mock = f"{continue_input}\n"

    with patch(
        "calimag.nwb_converter.NwbConverter.Convert2NWB",
        side_effect=return_nwb_if_y,
    ):
        runner = CliRunner()
        result = runner.invoke(cli, [str(config_filepath)], input=input_mock)

    def assert_output(result):
        assert (
            "Experiment version: "
            f"{config.get('DataInput').get('experiment_version_mandatory')}"
            in result.output
        )
        assert "Using the following configuration file:\n"
        f"{str(config_filepath.absolute())}" in result.output
        assert "The resulting NWB file will be written at the following location:\n"
        f"{str(nwb_test_path.absolute())}" in result.output
        assert "Aborted!" not in result.output

    try:
        assert result.exit_code == 0
        assert f"Calimag version {calimag.__version__}" in result.output

        if empty_fields:
            assert "The following fields in the config are empty:" in result.output
            if empty_fields_input.lower() == "y":
                if continue_input.lower() == "y":
                    assert_output(result)
                else:
                    assert "Aborted!" in result.output
            else:
                assert "Aborted!" in result.output
        else:
            if continue_input.lower() == "y":
                assert_output(result)
            else:
                assert "Aborted!" in result.output

    finally:
        if nwb_test_path.exists():
            nwb_test_path.unlink()
        assert not nwb_test_path.exists()
        lock.release()
        lock_path.unlink()
        assert not lock_path.exists()
        GetConfig.cache_clear()
