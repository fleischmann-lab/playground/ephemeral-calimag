"""
Workaround to save ophys data from Suite2p NumPy files.

Code borrowed from Suite2p:
https://github.com/MouseLand/suite2p/blob/main/suite2p/io/nwb.py
TODO: File to be removed when the following NWB issues are fixed:
- https://github.com/NeurodataWithoutBorders/pynwb/issues/1301
- https://github.com/NeurodataWithoutBorders/pynwb/issues/1297
"""

import os
from pathlib import Path
from typing import List

import numpy as np
from pynwb.base import Images  # type: ignore
from pynwb.image import GrayscaleImage  # type: ignore
from pynwb.ophys import (  # type: ignore
    Fluorescence,
    ImageSegmentation,
    OpticalChannel,
    RoiResponseSeries,
)

from calimag.config import remove_undefined_config_args
from calimag.parsers import MicroscopeParser, get_timedelta_from_start_time

NWB = True


# TODO: At the moment this function is not tested since this file should be removed
# at some point, should it be tested anyway?
def save_nwb(save_folder, nwbfile, xml_filepath, config):
    """Convert folder with plane folders to NWB format."""
    # plane_folders = natsorted(
    #     [
    #         f.path
    #         for f in os.scandir(save_folder)
    #         if f.is_dir() and f.name[:5] == "plane"
    #     ]
    # )
    plane_folders = np.sort(
        [
            Path(f.path)
            for f in os.scandir(save_folder)
            if f.is_dir() and "plane" in f.name.lower()
        ]
    ).tolist()
    ops1 = [
        np.load(f.joinpath("ops.npy"), allow_pickle=True).item() for f in plane_folders
    ]
    nchannels = [ops["nchannels"] for ops in ops1]

    if NWB and not ops1[0]["mesoscan"]:
        if len(ops1) > 1:
            multiplane = True
        else:
            multiplane = False

        ops = ops1[0]

        # ### INITIALIZE NWB FILE
        # nwbfile = NWBFile(
        #     session_description='suite2p_proc',
        #     identifier=str(ops['data_path'][0]),
        #     session_start_time=(ops['date_proc'] if 'date_proc' in ops
        #                         else datetime.datetime.now())
        # )
        # print(nwbfile)

        ImagingSection = config.get("Imaging")
        DeviceSection = ImagingSection.get("Device")
        device_args = {
            "name": DeviceSection.get("Type_mandatory"),
            "description": DeviceSection.get("Description"),
            "manufacturer": DeviceSection.get("Manufacturer"),
        }
        device = nwbfile.create_device(**remove_undefined_config_args(device_args))
        optical_device_args = {
            "name": "OpticalChannel",
            "description": ImagingSection.get("OpticalChannelDescription_mandatory"),
            "emission_lambda": ImagingSection.get("emission_lambda"),
        }
        optical_channel = OpticalChannel(
            **remove_undefined_config_args(optical_device_args)
        )
        parser = MicroscopeParser(filepath=xml_filepath)
        imaging_plane_args = {
            "name": "ImagingPlane",
            "optical_channel": optical_channel,
            "imaging_rate": ops["fs"],
            "description": ImagingSection.get("PlaneDescription_mandatory"),
            "device": device,
            "excitation_lambda": parser.get_excitation_lambda(),
            "indicator": ImagingSection.get("CalciumIndicator_mandatory"),
            "location": ImagingSection.get("ImagingLocation_mandatory"),
            "grid_spacing": ImagingSection.get("grid_spacing"),
            "grid_spacing_unit": ImagingSection.get("grid_spacing_unit"),
            "reference_frame": ImagingSection.get("reference_frame"),
        }
        imaging_plane = nwbfile.create_imaging_plane(
            **remove_undefined_config_args(imaging_plane_args)
        )

        # # link to external data
        # image_series = TwoPhotonSeries(
        #     name="TwoPhotonSeries",
        #     dimension=[ops["Ly"], ops["Lx"]],
        #     external_file=(ops["filelist"] if "filelist" in ops else [""]),
        #     imaging_plane=imaging_plane,
        #     starting_frame=[0],
        #     format="external",
        #     starting_time=0.0,
        #     rate=ops["fs"] * ops["nplanes"],
        # )
        # nwbfile.add_acquisition(image_series)

        # processing
        img_seg = ImageSegmentation()
        ps = img_seg.create_plane_segmentation(
            name="PlaneSegmentation",
            description="suite2p output",
            imaging_plane=imaging_plane,
            # reference_images=image_series,
        )
        ophys_module = nwbfile.create_processing_module(
            name="ophys", description="optical physiology processed data"
        )
        ophys_module.add(img_seg)

        file_strs = ["F.npy", "Fneu.npy", "spks.npy"]
        file_strs_chan2 = ["F_chan2.npy", "Fneu_chan2.npy"]
        traces, traces_chan2 = [], []
        nchannels = check_number_of_channels(
            nchannels_s2p=nchannels, scope_filepath=xml_filepath
        )
        Nfr = np.array([ops["nframes"] for ops in ops1]).max()
        ncells = np.zeros(len(ops1), dtype=np.int_)
        for iplane, ops in enumerate(ops1):
            if iplane == 0:
                iscell = np.load(plane_folders[iplane].joinpath("iscell.npy"))
                for fstr in file_strs:
                    traces.append(np.load(plane_folders[iplane].joinpath(fstr)))
                if nchannels > 1:
                    for fstr in file_strs_chan2:
                        traces_chan2.append(
                            np.load(plane_folders[iplane].joinpath(fstr))
                        )
                PlaneCellsIdx = iplane * np.ones(len(iscell))
            else:
                iscell = np.append(
                    iscell,
                    np.load(plane_folders[iplane].joinpath("iscell.npy")),
                    axis=0,
                )
                for i, fstr in enumerate(file_strs):
                    trace = np.load(plane_folders[iplane].joinpath(fstr))
                    if trace.shape[1] < Nfr:
                        fcat = np.zeros(
                            (trace.shape[0], Nfr - trace.shape[1]), "float32"
                        )
                        trace = np.concatenate((trace, fcat), axis=1)
                    traces[i] = np.append(traces[i], trace, axis=0)
                if nchannels > 1:
                    for i, fstr in enumerate(file_strs_chan2):
                        traces_chan2[i] = np.append(
                            traces_chan2[i],
                            np.load(plane_folders[iplane].joinpath(fstr)),
                            axis=0,
                        )
                PlaneCellsIdx = np.append(
                    PlaneCellsIdx, iplane * np.ones(len(iscell) - len(PlaneCellsIdx))
                )

            stat = np.load(
                plane_folders[iplane].joinpath("stat.npy"), allow_pickle=True
            )
            ncells[iplane] = len(stat)
            for n in range(ncells[iplane]):
                if multiplane:
                    pixel_mask = np.array(
                        [
                            stat[n]["ypix"],
                            stat[n]["xpix"],
                            iplane * np.ones(stat[n]["npix"]),
                            stat[n]["lam"],
                        ]
                    )
                    ps.add_roi(voxel_mask=pixel_mask.T)
                else:
                    pixel_mask = np.array(
                        [stat[n]["ypix"], stat[n]["xpix"], stat[n]["lam"]]
                    )
                    ps.add_roi(pixel_mask=pixel_mask.T)
            # ncells_all += ncells

        ps.add_column("iscell", "two columns - iscell & probcell", iscell)

        # rt_region = ps.create_roi_table_region(
        #     region=list(np.arange(0, ncells_all)), description="all ROIs"
        # )
        rt_region = []
        for iplane, ops in enumerate(ops1):
            if iplane == 0:
                rt_region.append(
                    ps.create_roi_table_region(
                        region=list(
                            np.arange(0, ncells[iplane]),
                        ),
                        description=f"ROIs for plane_{int(iplane) + 1}",
                    )
                )
            else:
                rt_region.append(
                    ps.create_roi_table_region(
                        region=list(
                            np.arange(
                                np.sum(ncells[:iplane]),
                                ncells[iplane] + np.sum(ncells[:iplane]),
                            )
                        ),
                        description=f"ROIs for plane_{int(iplane) + 1}",
                    )
                )

        # FLUORESCENCE (all are required)
        # file_strs = ["F.npy", "Fneu.npy", "spks.npy"]
        name_strs = ["Fluorescence", "Neuropil", "Deconvolved"]
        name_strs_chan2 = ["Fluorescence_chan2", "Neuropil_chan2"]

        # Create Fluorescence Neuropil, and Deconvolved containers
        for i, (fstr, nstr) in enumerate(zip(file_strs, name_strs)):
            for iplane, ops in enumerate(ops1):

                # Get timestamps from microscope XLM file
                time_frames = parser.get_frames_timestamps_from_xml(
                    plane_number=int(iplane) + 1
                )

                roi_resp_series = RoiResponseSeries(
                    name=f"Plane_{int(iplane) + 1}",
                    data=np.transpose(traces[i][PlaneCellsIdx == iplane]),
                    rois=rt_region[iplane],
                    unit="lumens",
                    timestamps=get_timedelta_from_start_time(
                        start_time=nwbfile.timestamps_reference_time,
                        timestamps=time_frames,
                    ),
                )

                if iplane == 0:
                    fl = Fluorescence(roi_response_series=roi_resp_series, name=nstr)
                else:
                    fl.add_roi_response_series(roi_response_series=roi_resp_series)

            ophys_module.add(fl)

        if nchannels > 1:
            for i, (fstr, nstr) in enumerate(zip(file_strs_chan2, name_strs_chan2)):
                for iplane, ops in enumerate(ops1):

                    # Get timestamps from microscope XLM file
                    time_frames = parser.get_frames_timestamps_from_xml(
                        plane_number=int(iplane) + 1
                    )

                    roi_resp_series = RoiResponseSeries(
                        name=f"Plane_{int(iplane) + 1}",
                        data=np.transpose(traces_chan2[i][PlaneCellsIdx == iplane]),
                        rois=rt_region[iplane],
                        unit="lumens",
                        timestamps=get_timedelta_from_start_time(
                            start_time=nwbfile.timestamps_reference_time,
                            timestamps=time_frames,
                        ),
                    )

                    if iplane == 0:
                        fl = Fluorescence(
                            roi_response_series=roi_resp_series, name=nstr
                        )
                    else:
                        fl.add_roi_response_series(roi_response_series=roi_resp_series)

                ophys_module.add(fl)

        # BACKGROUNDS
        # (meanImg, Vcorr and max_proj are REQUIRED)
        bg_strs = ["meanImg", "Vcorr", "max_proj", "meanImg_chan2"]
        nplanes = ops["nplanes"]
        for iplane in range(nplanes):
            images = Images("Backgrounds_%d" % iplane)
            for bstr in bg_strs:
                if bstr in ops:
                    if bstr == "Vcorr" or bstr == "max_proj":
                        img = np.zeros((ops["Ly"], ops["Lx"]), np.float32)
                        img[
                            ops["yrange"][0] : ops["yrange"][-1],
                            ops["xrange"][0] : ops["xrange"][-1],
                        ] = ops[bstr]
                    else:
                        img = ops[bstr]
                    images.add_image(GrayscaleImage(name=bstr, data=img))

            ophys_module.add(images)

        # with NWBHDF5IO(os.path.join(save_folder, 'ophys.nwb'), 'w') as fio:
        #     fio.write(nwbfile)

        return nwbfile

    else:
        print('pip install pynwb OR don"t use mesoscope recording')


def check_number_of_channels(nchannels_s2p: List[int], scope_filepath: Path) -> int:
    """Check number of channels between the scope XML file and Suite2p's files."""
    assert (
        len(set(nchannels_s2p)) == 1
    ), "The number of channels in the Suite2p files is not homogeneous for each plane"
    nchannels = list(set(nchannels_s2p))[0]
    parser = MicroscopeParser(filepath=scope_filepath)
    assert (
        parser.get_number_of_channels() == nchannels
    ), "The number of channels is not the same between "
    "the NPY files and the XML scope file"

    return nchannels
