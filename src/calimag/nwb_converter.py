"""Main class to convert raw data to NWB file format."""
import re
import warnings
from pathlib import Path

from pynwb import NWBHDF5IO, TimeSeries  # type: ignore
from pynwb.file import NWBFile, Subject  # type: ignore

import calimag.errors as err
from calimag import suite2p_parser
from calimag.config import GetConfig, remove_undefined_config_args
from calimag.parsers import TeensyParser, get_timedelta_from_start_time


class NwbConverter:
    """Main class to convert to standard NWB file format."""

    def __init__(
        self,
        config_filepath: Path,
    ) -> None:
        """Create NWB when instantiating the class."""
        self.config = GetConfig(config_filepath=config_filepath)
        DataInputSection = self.config.get("DataInput")
        if DataInputSection:
            self.microscope_filepath = DataInputSection.get(
                "microscope_filepath_mandatory"
            )
            self.teensy_filepath = DataInputSection.get("teensy_filepath_mandatory")
            self.suite2pNpyPath = DataInputSection.get("suite2pNpy_directory_mandatory")
            # self.TwoPhotonSeriesUrlOrPath = self.config.get("DataInput").get(
            #     "TwoPhotonSeriesUrlOrPath"
            # )
            self.nwb_output_path = DataInputSection.get("nwb_output_filepath_mandatory")

        if self.config.get("Subject"):
            subject = Subject(**self.config.get("Subject"))
        else:
            subject = None
        NWBFileSection = self.config.get("NWBFile")
        if NWBFileSection:
            nwbfile_args = {
                "session_description": NWBFileSection.get(
                    "session_description_mandatory"
                ),
                "identifier": NWBFileSection.get("identifier"),
                "session_start_time": NWBFileSection.get("session_start_time"),
                "lab": NWBFileSection.get("lab"),
                "institution": NWBFileSection.get("institution"),
                "subject": subject,
                "experimenter": NWBFileSection.get("experimenter"),
                "experiment_description": NWBFileSection.get("experiment_description"),
            }
            self.nwbfile = NWBFile(**remove_undefined_config_args(nwbfile_args))

    def ProcessTimeSeries(
        self, teensy_filepath: Path, microscope_filepath: Path
    ) -> NWBFile:
        """Get time series."""
        print("Processing timeseries data...")
        DataInputSection = self.config.get("DataInput")
        if not DataInputSection:
            raise err.ConfigMissingItem(
                "Mandatory [DataInput] section in config file not found"
            )
        parser = TeensyParser(
            experiment_version=DataInputSection.get("experiment_version_mandatory"),
            filepath=teensy_filepath,
        )
        data_df = parser.get_behavioral_data_from_teensy(
            microscope_filepath=microscope_filepath,
        )

        for col in data_df.columns:
            unit_pattern = re.compile(r"\[.*?\]")
            ColName = re.sub(unit_pattern, "", col).strip()
            pattern_search = re.search(unit_pattern, col)
            if pattern_search:
                ColUnit = pattern_search.group()[1:-1]
            else:
                ColUnit = "No unit"
                warnings.warn(
                    f"No unit found for the parsed '{col}' data from the Teensy file"
                )
            data_ts = TimeSeries(
                name=ColName,
                data=list(data_df[col]),
                timestamps=get_timedelta_from_start_time(
                    start_time=self.nwbfile.timestamps_reference_time,
                    timestamps=data_df.index.to_series(),
                ),
                unit=ColUnit,
            )

            if "odor" in ColName.lower():
                # Add stimulus to NWB file
                self.nwbfile.add_stimulus(data_ts)

            elif ColName == "trial":
                # Add trials to NWB file
                for trial in data_df.groupby(col).groups:
                    start_time = (
                        data_df.loc[data_df[col] == trial].index[0]
                        - self.nwbfile.timestamps_reference_time
                    )
                    stop_time = (
                        data_df.loc[data_df[col] == trial].index[-1]
                        - self.nwbfile.timestamps_reference_time
                    )
                    self.nwbfile.add_trial(
                        start_time=start_time.total_seconds(),
                        stop_time=stop_time.total_seconds(),
                    )
            else:
                self.nwbfile.add_acquisition(data_ts)

        print("Processing timeseries data: done!")
        return self.nwbfile

    # def ProcessImagingData(
    #     self,
    #     xml_filepath: str,
    #     nwb2p_filepath: str,
    #     TwoPhotonSeriesUrlOrPath: List,
    # ) -> NWBFile:
    #     """Get imaging data"""

    #     print("Processing imaging data...")

    #     root = ET.parse(xml_filepath).getroot()
    #     Sequence = root.find("Sequence")
    #     if Sequence:
    #         PlaneIndexes = Sequence.findall("Frame")
    #     else:
    #         raise XmlElementNotFound("'Sequence' element not found in XML file")
    #     SeqFrameFile = root.find("Sequence/Frame/File")
    #     if SeqFrameFile is not None:
    #         channelName = SeqFrameFile.get("channelName")
    #     else:
    #         raise XmlElementNotFound(
    #             "Sequence tree: 'Sequence>Frame>File'" " not found in XML file"
    #         )
    #     excitation_lambda_str = ".//PVStateValue[@key='laserWavelength']/IndexedValue"
    #     excitation_lambda_elem = root.find(excitation_lambda_str)
    #     if excitation_lambda_elem is not None:
    #         excitation_lambda_val = excitation_lambda_elem.get("value")
    #         assert isinstance(excitation_lambda_val, str)
    #         excitation_lambda = float(excitation_lambda_val)
    #     else:
    #         raise XmlElementNotFound(
    #             f"Xpath sequence {excitation_lambda_str} not found"
    #         )
    #     framePeriodElem = root.find("PVStateShard/PVStateValue[@key='framePeriod']")
    #     if framePeriodElem is not None:
    #         framePeriodVal = framePeriodElem.get("value")
    #         assert isinstance(framePeriodVal, str)
    #         framePeriod: Optional[float] = float(framePeriodVal)
    #     else:
    #         warnings.warn("'framePeriod' element not found in XML file")
    #         framePeriod = None
    #     micronsPerPixel = root.find(".//PVStateValue[@key='micronsPerPixel']")
    #     if micronsPerPixel is not None:
    #         micronsPerPixelArrayStr = [
    #             micronsPerPixel.find("IndexedValue[@index='XAxis']"),
    #             micronsPerPixel.find("IndexedValue[@index='YAxis']"),
    #             micronsPerPixel.find("IndexedValue[@index='ZAxis']"),
    #         ]
    #         micronsPerPixelArray: Optional[List[float]] = []
    #         for elem in micronsPerPixelArrayStr:
    #             if elem is not None:
    #                 val = elem.get("value")
    #                 assert isinstance(val, str)
    #                 micronsPerPixelArray.append(float(val))
    #             else:
    #                 warnings.warn(
    #                     "'IndexedValue' of 'micronsPerPixel' element"
    #                     " not found in XML file"
    #                 )
    #                 micronsPerPixelArray = None
    #                 break
    #     else:
    #         warnings.warn("'micronsPerPixel' element not found in XML file")
    #         micronsPerPixelArray = None

    #     SectionName = "Imaging"
    #     device = Device(name=GetConfig(SectionName, "device"))
    #     self.nwbfile.add_device(device)
    #     OpticalChannelDesc = GetConfig(SectionName,
    #     "OpticalChannelDescription_mandatory")
    #     ImagePixelDimensionStr = root.find(".//PVStateValue[@key='pixelsPerLine']")
    #     if ImagePixelDimensionStr is not None:
    #         ImagePixelDimensionVal = ImagePixelDimensionStr.get("value")
    #         assert isinstance(ImagePixelDimensionVal, str)
    #         ImagePixelDimension: Optional[int] = int(ImagePixelDimensionVal)
    #     else:
    #         warnings.warn("'pixelsPerLine' element not found in XML")
    #         ImagePixelDimension = None

    #     optical_channel = OpticalChannel(
    #         name=channelName,
    #         description=OpticalChannelDesc,
    #         emission_lambda=float(GetConfig(SectionName,
    #         "emission_lambda_mandatory")),
    #     )
    #     ophys_module = self.nwbfile.create_processing_module(
    #         name="ophys", description="optical physiology processed data"
    #     )
    #     img_seg = ImageSegmentation()

    #     for PlaneIndex, PlaneItem in enumerate(PlaneIndexes):
    #         imaging_plane = self.nwbfile.create_imaging_plane(
    #             name=f"Plane_{PlaneItem.get('index')}",
    #             optical_channel=optical_channel,
    #             imaging_rate=1 / framePeriod,
    #             description=f"Imaging plane #{PlaneItem.get('index')}",
    #             device=device,
    #             excitation_lambda=excitation_lambda,
    #             indicator=GetConfig(SectionName, "CalciumIndicator_mandatory"),
    #             location=GetConfig(SectionName, "ImagingLocation_mandatory"),
    #             grid_spacing=micronsPerPixelArray,
    #             grid_spacing_unit=GetConfig(SectionName, "grid_spacing_unit"),
    #         )

    #         # # Store data in the NWB file
    #         # image_series1 = TwoPhotonSeries(
    #         #     name="TwoPhotonSeries1",
    #         #     data=np.ones((1000, 100, 100)),
    #         #     imaging_plane=imaging_plane,
    #         #     rate=1 / framePeriod,
    #         #     unit="normalized amplitude",
    #         # )

    #         # Link to external data
    #         image_series = TwoPhotonSeries(
    #             name=f"TwoPhotonSeries{PlaneItem.get('index')}",
    #             dimension=[ImagePixelDimension, ImagePixelDimension],
    #             external_file=[TwoPhotonSeriesUrlOrPath[PlaneIndex]],
    #             imaging_plane=imaging_plane,
    #             format="external",
    #             starting_frame=[0],
    #             # starting_time=0.0,  # TODO: First value of timestamp vector
    #             rate=1 / framePeriod,
    #             # pmt_gain= # TODO: Add Photomultiplier gain since it's in the XML
    #             # timestamps= # TODO: Add timestamps from
    #             # MicroscopeParsing.get_frames_timestamps_from_xml()
    #         )
    #         self.nwbfile.add_acquisition(image_series)

    #         img_seg.create_plane_segmentation(
    #             name=f"PlaneSegmentation{PlaneItem.get('index')}",
    #             description="Output from segmenting "
    #             f"imaging plane #{PlaneItem.get('index')}",
    #             imaging_plane=imaging_plane,
    #             reference_images=image_series,
    #         )
    #     ophys_module.add(img_seg)

    #     print("Processing imaging data: done!")
    #     return self.nwbfile

    # def exportSuite2pNWB(
    #     self,
    #     nwb2p_filepath: str,
    #     nwb_output_path: Optional[str] = None,
    # ) -> NWBFile:

    #     print("Exporting data from segmentation file...")
    #     ophys_module = self.nwbfile.processing["ophys"]
    #     manager = get_manager()
    #     with NWBHDF5IO(nwb2p_filepath, "r", manager=manager) as io:
    #         nwb2p = io.read()
    #         # read_nwbfile.processing["ophys"].data_interfaces[
    #           "Fluorescence"].roi_response_series["Fluorescence"].rois
    #         for data_interface in nwb2p.processing["ophys"].data_interfaces.keys():
    #             if (
    #                 data_interface == "ImageSegmentation"
    #                 or "Backgrounds_" in data_interface  # TODO: Add Background infos?
    #             ):
    #                 continue
    #             else:
    #                 ophys_module.add(
    #                     nwb2p.processing["ophys"].data_interfaces.get(data_interface)
    #                 )
    #                 # Workaround on the impossibility to write container which
    #                 # originally belonged to another NWB file
    #                 # ValueError: Can't change container_source once set
    # https://github.com/NeurodataWithoutBorders/pynwb/issues/668#issuecomment-643513068
    #                 ophys_module.data_interfaces.get(
    #                     data_interface
    #                 )._AbstractContainer__container_source = nwb_output_path
    #                 for field in ophys_module.data_interfaces.get(
    #                     data_interface
    #                 ).fields:
    #                     ophys_module.data_interfaces.get(data_interface)[
    #                         field
    #                     ]._AbstractContainer__container_source = nwb_output_path

    #         # _Warning_: both files, the segmentation NWB file from Suite2p
    #         # AND the final NWB file need to be open to be able to copy
    #         # data containers from one file to the other, otherwise it works
    #         # as long as the object is in memory but you get broken links
    #         # when you write the file on disk
    #         if nwb_output_path:
    #             with NWBHDF5IO(nwb_output_path, "w", manager=manager) as io:
    #                 # io.write(self.nwbfile, link_data=False)
    #                 io.write(self.nwbfile)

    #     print("Exporting data from segmentation file: done!")
    #     return self.nwbfile

    def Convert2NWB(
        self,
    ) -> NWBFile:
        """Convert raw data to a standard NWB object."""
        print("Converting data to NWB...")
        self.nwbfile = self.ProcessTimeSeries(
            teensy_filepath=self.teensy_filepath,
            microscope_filepath=self.microscope_filepath,
        )
        # self.nwbfile = self.ProcessImagingData(
        #     xml_filepath=self.microscope_filepath,
        #     nwb2p_filepath=self.segmentation_fp,
        #     TwoPhotonSeriesUrlOrPath=self.TwoPhotonSeriesUrlOrPath,
        # )
        # self.exportSuite2pNWB(
        #     nwb2p_filepath=self.segmentation_fp, nwb_output_path=nwb_output_path
        # )

        # Workaround to save ophys data from Suite2p NumPy files
        print("Parsing NumPy files from Suite2p...")
        self.nwbfile = suite2p_parser.save_nwb(
            save_folder=self.suite2pNpyPath,
            nwbfile=self.nwbfile,
            xml_filepath=self.microscope_filepath,
            config=self.config,
        )
        print("Parsing NumPy files from Suite2p: done!")

        # Write data to disk
        with NWBHDF5IO(str(self.nwb_output_path), "w") as io:
            io.write(self.nwbfile, link_data=False)

        print("Converting data to NWB: done!")
        return self.nwbfile
