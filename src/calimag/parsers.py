"""Data parsing classes."""
from __future__ import annotations

import re
import warnings
import xml.etree.ElementTree as ET
from decimal import Decimal
from functools import lru_cache
from pathlib import Path
from typing import Optional, cast

import numpy as np
import pandas as pd
from chardet.universaldetector import UniversalDetector
from dateutil.parser import parser
from packaging import version
from tqdm import tqdm  # type: ignore

import calimag.constants as CONST
import calimag.errors as err


class Parser:
    """Parser parent class."""

    def __init__(self, filepath: Path, experiment_version: str = None):
        self.experiment_version = experiment_version
        self.filepath = filepath


class MicroscopeParser(Parser):
    """Parse XML file from the microscope."""

    def __init__(self, filepath: Path, experiment_version: str = None):
        self.root_xml = ET.parse(filepath).getroot()
        super().__init__(experiment_version=experiment_version, filepath=filepath)

    def get_version(self) -> str:
        """Get the version number of the file."""
        version_nb = self.root_xml.get("version")
        if not version_nb:
            raise err.UnsupportedVersion("Unable to get a version from your file")
        return version_nb

    def get_number_of_channels(self) -> int:
        """Get the number of channels used in the experiment."""
        nchannels_elems = self.root_xml.findall("./Sequence/Frame/File[@channel]")
        if not nchannels_elems:
            raise err.UnsupportedVersion(
                "Unable to get the number of channels in the microscope file"
            )
        nchannels = len(set([elem.get("channel") for elem in nchannels_elems]))
        return nchannels

    def get_excitation_lambda(self) -> float:
        """Get the laser excitation wavelength."""
        excitation_lambda_str = ".//PVStateValue[@key='laserWavelength']/IndexedValue"
        excitation_lambda_elem = self.root_xml.find(excitation_lambda_str)
        if excitation_lambda_elem is not None:
            excitation_lambda_val = excitation_lambda_elem.get("value")
            assert isinstance(excitation_lambda_val, str)
            excitation_lambda = float(excitation_lambda_val)
        else:
            raise err.XmlElementNotFound(
                f"Xpath sequence {excitation_lambda_str} not found"
            )
        return excitation_lambda

    @lru_cache(maxsize=None)  # Keep return value in cache
    def get_session_start_time(self, timezone: str = CONST.TIMEZONE) -> pd.Timestamp:
        """Get the first timestamp from the Microscope."""
        # Get the data
        file_date = self.root_xml.get("date")
        p = parser()
        if file_date:
            file_date_parsed = p.parse(file_date).date()
        else:
            raise err.SessionStartTimeError(
                "Unable to get session start time from the XML file"
            )

        # Get the 1st data point
        first_time_sequence = self.root_xml.find("./Sequence[@cycle='1']")
        if first_time_sequence:
            first_time_point = first_time_sequence.get("time")
        else:
            raise err.XmlElementNotFound("Element not found in XML microscope file")

        # Get the first absolute delta time [sec]
        first_abs_time_frame_xml = first_time_sequence.find(
            "./Frame[@relativeTime='0'][@index='1']"
        )
        if first_abs_time_frame_xml:
            first_abs_time_frame = cast(
                str, first_abs_time_frame_xml.get("absoluteTime")
            )
        else:
            raise err.XmlElementNotFound("Element not found in XML microscope file")

        # Return the combination of date + 1st data point + timezone
        session_start_time = pd.Timestamp(
            "T".join([str(file_date_parsed), str(first_time_point)])
        )
        session_start_time = session_start_time + pd.Timedelta(
            value=first_abs_time_frame + "sec"
        )
        # Replace fixed time zone by system time zone?
        # from dateutil.tz import tzlocal
        # session_start_time_tz = session_start_time.tz_localize(tz=tzlocal())
        session_start_time_tz = session_start_time.tz_localize(tz=timezone)

        return session_start_time_tz

    @lru_cache(maxsize=None)
    def get_frames_timestamps_from_xml(
        self,
        plane_number: int,
        timezone: Optional[str] = CONST.TIMEZONE,
    ) -> pd.Series:
        """Parse XML file to get the frames timestamps.

        Parameters
        ----------
        plane_number : int
            Tiff image index recorded for each frame
        timezone : str, optional
            Time zone to localize the returned timestamps. If no argument is passed,
            it defaults to the `EST` time zone

        Returns
        -------
        pd.Series
            Vector of timestamps
        """
        # Parsing according to version of the file
        version_nb = self.get_version()
        if version.parse(version_nb) == version.parse("5.4.64.700"):

            # Get file date
            session_start_time = self.get_session_start_time()

            # Frame index
            if not isinstance(plane_number, int):
                raise IndexError("Plane_number must be an integer")
            frame_indexes = self.root_xml.findall("./Sequence/Frame[@index]")
            max_frame_indexes = max(
                [int(cast(str, item.get("index"))) for item in frame_indexes]
            )

            assert isinstance(max_frame_indexes, int)
            if plane_number > max_frame_indexes or plane_number < 1:
                raise IndexError(
                    f"Parsing timestamps for frame index number: {plane_number}"
                    f" out of [1, {max_frame_indexes}] in XML file."
                )

            first_time_sequence_xml = self.root_xml.find("./Sequence[@cycle='1']")
            if first_time_sequence_xml:
                first_time_sequence = first_time_sequence_xml.get("time")
            else:
                raise err.XmlElementNotFound("Element not found in XML microscope file")
            sequence_full_time = pd.Timestamp(
                "T".join([str(session_start_time.date()), str(first_time_sequence)])
            )

            # Parse frame recorded time
            sequences = self.root_xml.findall("./Sequence[@cycle]")
            time_frames = np.empty(shape=len(sequences), dtype=object)
            for idx, sequence in enumerate(sequences):
                absolute_time_txt_xml = sequence.find(
                    f"./Frame[@absoluteTime][@index='{plane_number}']"
                )
                if absolute_time_txt_xml:
                    absolute_time_txt = absolute_time_txt_xml.get("absoluteTime")
                    assert absolute_time_txt
                else:
                    raise err.XmlElementNotFound(
                        "Element not found in XML microscope file"
                    )

                time_frames[idx] = self._convert_txt2_timestamp(
                    absolute_time_txt=absolute_time_txt,
                    sequence_full_time=sequence_full_time,
                    timezone=timezone,
                )

            timestamps = pd.Series(time_frames)
            return timestamps

        else:
            raise err.UnsupportedVersion("The version of your file is unsupported")

    def get_trials_timestamps_from_xml(
        self,
        timezone: Optional[str] = CONST.TIMEZONE,
    ) -> pd.Series:
        """Get only the trials timestamps from microscope XML file."""
        version_nb = self.get_version()
        if version.parse(version_nb) == version.parse("5.4.64.700"):

            # Get file date
            session_start_time = self.get_session_start_time()

            first_time_sequence_xml = self.root_xml.find("./Sequence[@cycle='1']")
            if first_time_sequence_xml:
                first_time_sequence = first_time_sequence_xml.get("time")
            else:
                raise err.XmlElementNotFound("Element not found in XML microscope file")
            sequence_full_time = pd.Timestamp(
                "T".join([str(session_start_time.date()), str(first_time_sequence)])
            )
            # Parse frame recorded time
            sequences = self.root_xml.findall("./Sequence[@time]")
            time_frames = np.empty(shape=len(sequences), dtype=object)
            for idx, sequence in enumerate(sequences):
                absolute_time_txt_xml = sequence.find("./Frame[@relativeTime='0']")
                if absolute_time_txt_xml:
                    absolute_time_txt = absolute_time_txt_xml.get("absoluteTime")
                    assert absolute_time_txt
                else:
                    raise err.XmlElementNotFound(
                        "Element not found in XML microscope file"
                    )

                time_frames[idx] = self._convert_txt2_timestamp(
                    absolute_time_txt=absolute_time_txt,
                    sequence_full_time=sequence_full_time,
                    timezone=timezone,
                )

            timestamps = pd.Series(time_frames)
            return timestamps

        else:
            raise err.UnsupportedVersion("The version of your file is unsupported")

    def _convert_txt2_timestamp(
        self,
        absolute_time_txt: str,
        sequence_full_time: pd.Timestamp,
        timezone: Optional[str] = CONST.TIMEZONE,
    ) -> pd.Timestamp:
        # TODO: Function still to be tested

        # Round the absolute time to the nanosecond or pad it to the 9th decimal
        # The `Decimal()` function is necessary to keep the precision
        # and choose the number of decimals during rounding
        # (otherwise you can only choose the number of digits)
        # decimal.getcontext().rounding= decimal.ROUND_05UP
        if absolute_time_txt:
            absolute_time_d9 = str(round(Decimal(absolute_time_txt), 9))
        else:
            raise err.XmlElementNotFound("Element not found in XML microscope file")

        if "." in absolute_time_d9:
            # if absolute_time_rd > absolute_time_rd.to_integral():
            # Split the relative time text in seconds and nanoseconds
            absolute_time = pd.Timedelta(
                value=int(absolute_time_d9.split(".")[0]), unit="seconds"
            ) + pd.Timedelta(
                value=int(absolute_time_d9.split(".")[1]), unit="nanoseconds"
            )
        else:
            absolute_time = pd.Timedelta(value=int(absolute_time_d9), unit="seconds")
        timestamp = (sequence_full_time + absolute_time).tz_localize(tz=timezone)
        return timestamp


class TeensyParser(Parser):
    """Parse behavioral data from the Teensy board."""

    def __init__(self, experiment_version: str, filepath: Path):
        """
        Initiate the TeensyParser class.

        The encoding detetction is used many times in the class which is why
        it's been added as a attribute.
        """
        super().__init__(experiment_version=experiment_version, filepath=filepath)
        self.encoding = self.detect_encoding()

    def get_behavioral_data_from_teensy(
        self,
        microscope_filepath: Path,
    ) -> pd.DataFrame:
        """
        Parse Teensy file to get the behavioral data.

        Typical line to parse: 'oData,418,8,319,1157,1'

        oData = meaningless
        418 = time from start of trial
        8 = odor number
        319 = flow sensor value
        1157 = wheel value
        1 = trial number
        """
        data_ini = []
        data_end = []
        data = []
        corrupted_msg = "Your file looks corrupted"

        with open(self.filepath, encoding=self.encoding) as fid:
            rows = fid.readlines()

        idx_ini1: Optional[int] = None
        idx_ini2: Optional[int] = None

        assert self.experiment_version, "'experiment_version' argument undefined"
        teensy_header = CONST.TEENSY_HEADER.get(self.experiment_version)
        if teensy_header is None:
            raise err.UnsupportedVersion(
                f"The version `{self.experiment_version}` of your Teensy file "
                "is unsupported, the currently supported versions are the following: "
                f"{CONST.EXPERIMENT_VERSIONS}."
            )

        # Parsing according to version of the file
        if self.experiment_version == CONST.EXPERIMENT_VERSIONS[3]:
            # Find start of the data
            for idx, row in enumerate(rows):
                if row.strip() == "a1>":
                    idx_ini1 = idx
                    break
            assert idx_ini1 is not None, corrupted_msg  # TODO: unclear why needed
            first_line_pattern = re.compile(r"^1,\d+,\d+,\d+,\d+,\d+")
            for idx, row in enumerate(rows[idx_ini1:]):
                if re.match(first_line_pattern, row):
                    idx_ini2 = idx
                    break
            assert idx_ini2 is not None, corrupted_msg
            data = rows[idx_ini1 + idx_ini2 :]

            # Filter rows which are not data
            # TODO: some corrupted lines that start with 0s still have previous trials
            # temporary workaround is to ignore those 0 lines
            line_pattern = re.compile(r"^[1-9]\d*,\d+,\d+,\d+,\d+,\d+")
            data = list(filter(lambda row: re.match(line_pattern, row), tqdm(data)))
            data = list(map(lambda row: row.split(","), tqdm(data)))

        elif self.experiment_version == CONST.EXPERIMENT_VERSIONS[2]:

            # Find start of the data
            for idx, row in enumerate(rows):
                if row.strip() == "a1>":
                    idx_ini1 = idx
                    break
            assert idx_ini1 is not None, corrupted_msg
            first_line_pattern = re.compile(r"^1,\d+,\d+")
            for idx, row in enumerate(rows[idx_ini1:]):
                if re.match(first_line_pattern, row):
                    idx_ini2 = idx
                    break
            assert idx_ini2 is not None, corrupted_msg
            data = rows[idx_ini1 + idx_ini2 :]

            # Filter rows which are not data
            line_pattern = re.compile(r"^\d+,\d+,\d+")
            data = list(filter(lambda row: re.match(line_pattern, row), tqdm(data)))
            data = list(map(lambda row: row.split(","), tqdm(data)))

        elif self.experiment_version in [
            CONST.EXPERIMENT_VERSIONS[1],
            CONST.EXPERIMENT_VERSIONS[4],
        ]:

            # Save indexes between data recording to filter in between data
            for idx, row in enumerate(rows):
                if "new odor #" in row:
                    data_ini.append(idx)
                elif "Finished odor;" in row:
                    data_end.append(idx)
            assert len(data_ini) == len(data_end), corrupted_msg

            # Get data in between starting and ending indexes
            for idx_ini, idx_end in zip(data_ini, data_end):
                data.extend(rows[idx_ini:idx_end])
            data = list(
                map(lambda row: row.rstrip(), tqdm(data))
            )  # Remove linefeed characters

            # Filter rows which are not data
            n_columns = len(teensy_header)
            line_pattern = re.compile(r"^.?oData(,\d+){%d}$" % (n_columns))
            if self.experiment_version == CONST.EXPERIMENT_VERSIONS[1]:
                line_pattern = re.compile(r"^.?oData(,\d+){5}$")
            if self.experiment_version == CONST.EXPERIMENT_VERSIONS[4]:
                line_pattern = re.compile(r"^.?oData(,\d+){6}$")
            data = list(filter(lambda row: re.match(line_pattern, row), tqdm(data)))

            # Convert to Pandas dataframe
            data = list(
                map(
                    lambda row: cast(str, re.sub(r"^.?oData,", "", row).split(",")),
                    tqdm(data),
                )
            )
            assert isinstance(data, list)
            assert data != [], "Extracted data from Teensy file is empty"

        elif self.experiment_version == CONST.EXPERIMENT_VERSIONS[0]:

            # Save indexes between data recording to filter in between data
            for idx, row in enumerate(rows):
                if "new odor #" in row:
                    data_ini.append(idx)
                elif "Finished odor;" in row:
                    data_end.append(idx)
            assert len(data_ini) == len(data_end), corrupted_msg

            # Get data in between starting and ending indexes
            for idx_ini, idx_end in zip(data_ini, data_end):
                data.extend(rows[idx_ini:idx_end])
            data = list(
                map(lambda row: row.rstrip(), tqdm(data))
            )  # Remove linefeed characters

            # Filter rows which are not data
            line_pattern = re.compile(r"^\d+,\d+,\d+,\d+,\d+,\d+")
            data = list(filter(lambda row: re.match(line_pattern, row), tqdm(data)))

            # Convert to Pandas dataframe
            data = list(
                map(
                    lambda row: cast(str, re.sub(r"^.?oData,", "", row).split(",")),
                    tqdm(data),
                )
            )
            assert isinstance(data, list)
            assert data != [], "Extracted data from Teensy file is empty"

        header_labels = [
            f"{item.get('name')} [{item.get('unit')}]" for item in teensy_header
        ]
        data_df = pd.DataFrame(data, columns=header_labels, dtype=np.float32)
        assert not data_df.empty, "Extracted dataframe from Teensy file is empty"

        # Convert units
        for col in teensy_header:
            conversion_factor = col.get("conversion_factor")
            if col.get("unit") != col.get("SI_unit") and conversion_factor != 1:
                col_current_name = f"{col.get('name')} [{col.get('unit')}]"
                col_new_name = f"{col.get('name')} [{col.get('SI_unit')}]"
                assert (
                    col_current_name in data_df
                ), f"Expected column '{col_current_name}' not found in Teensy"
                data_df[col_current_name] = (
                    data_df[col_current_name]
                    .apply(lambda x: x * conversion_factor)
                    .astype("float32")
                )
                data_df.rename(columns={col_current_name: col_new_name}, inplace=True)

        # Take care of incoherent values
        data_df.loc[
            data_df["wheel [rad]"] > CONST.WHEEL_MAX_ENCODING, "wheel [rad]"
        ] = np.nan
        # data_df["wheel_rate"] = data_df.wheel.shift(1) - data_df.wheel
        # data_df.wheel[data_df.wheel_rate > wheel_max_rate] = np.nan
        # data_df.drop("wheel_rate", axis=1, inplace=True)

        microscope_parser = MicroscopeParser(filepath=microscope_filepath)
        trials_timestamps = microscope_parser.get_trials_timestamps_from_xml()
        timestamps = create_aligned_timestamps(
            time_array=data_df["time [ms]"], external_trials_start=trials_timestamps
        )
        data_df["timestamps"] = timestamps
        data_df.set_index("timestamps", inplace=True, verify_integrity=True, drop=True)
        assert (
            data_df.index.is_monotonic_increasing
        ), "Timestamps are not monotonically increasing"

        # Clean useless columns
        data_df.drop("time [ms]", axis="columns", inplace=True)
        for col in data_df.columns:
            if "unknown" in col:
                assert isinstance(col, str)
                data_df.drop(columns=col, axis="columns", inplace=True)

        return data_df

    def detect_encoding(self) -> Optional[str]:
        """Detect encoding of the file."""
        with open(self.filepath, "rb") as fid:
            detector = UniversalDetector()
            for line in fid.readlines():
                detector.feed(line)
                if detector.done:
                    break
            detector.close()
        return detector.result.get("encoding")


def create_aligned_timestamps(
    time_array: pd.Series,
    external_trials_start: pd.Series,
) -> pd.Series:
    """Create timestamp as index."""
    dx_vect = np.diff(time_array)
    dx = dx_vect[0]  # Time difference between 2 samples
    reset_times = dx_vect < 0
    intra_trial_dx = dx_vect[~reset_times]
    time_by_trials = np.split(time_array, np.where(reset_times)[0] + 1)
    assert len(time_by_trials) == len(
        external_trials_start
    ), "Number of trials in time does not match length of the start times"
    assert np.all(
        intra_trial_dx > 0
    ), "Consecutive time between 2 samples is not increasing"
    assert np.all(
        intra_trial_dx == dx
    ), "Consecutive time between 2 samples is not homogeneous"

    # Find indexes where time reset to 1
    trials_intervals_idx_diff = np.where(np.abs(np.diff(time_array)) > dx)

    # Remove useless dimension + move the indexes of 1
    trials_intervals_idx = np.squeeze(trials_intervals_idx_diff) + 1
    trials_intervals_idx = np.insert(
        trials_intervals_idx, (0, len(trials_intervals_idx)), (0, len(time_array))
    )  # Add first & last index

    # Check that the number of trials match
    # The number of intervals is 1 more than the number of trials (last index)
    assert len(external_trials_start) + 1 == len(
        trials_intervals_idx
    ), "Something is wrong with the number of trials"

    for idx, trial_start in tqdm(
        enumerate(external_trials_start), total=len(external_trials_start)
    ):

        # Make trial time Start at 0 instead of 1
        delta_time = time_array[trials_intervals_idx[idx]]
        if delta_time > 0:
            time_array_trial = (
                time_array[trials_intervals_idx[idx] : trials_intervals_idx[idx + 1]]
                - delta_time
            )
        else:
            time_array_trial = time_array[
                trials_intervals_idx[idx] : trials_intervals_idx[idx + 1]
            ]
        assert not np.any(time_array_trial < 0), "Can't have negative trial times"

        # Initialize with first trial
        if idx == 0:
            timestamps = pd.Series(
                [
                    trial_start
                    + pd.Timedelta(
                        value=int(recorded_time),
                        unit="milliseconds",
                    )
                    for recorded_time in time_array_trial
                ]
            )

        else:
            # Truncate timestamps greater than the start of the next trial
            if timestamps.iloc[-1] > trial_start:
                dropped_data_pts = timestamps[timestamps >= trial_start]
                timestamps.drop(
                    timestamps[timestamps >= trial_start].index, inplace=True
                )
                warnings.warn(
                    f"Dropping {len(dropped_data_pts)} "
                    f"data points from trial {idx} "
                    "since they overflow the start of the next trial"
                )

            timestamps = pd.concat(
                [
                    timestamps,
                    pd.Series(
                        [
                            trial_start
                            + pd.Timedelta(
                                value=int(recorded_time),
                                unit="milliseconds",
                            )
                            for recorded_time in time_array_trial
                        ]
                    ),
                ],
                ignore_index=True,
            )
            assert isinstance(timestamps, pd.Series)
    assert (
        timestamps.is_monotonic_increasing
    ), "Timestamps are not monotonically increasing"
    return timestamps


def get_timedelta_from_start_time(
    start_time: pd.Timestamp,
    timestamps: pd.Series,
) -> np.ndarray:
    """Convert full timestamps to floats starting from the global file start time."""
    # Convert to Pandas which supports nanosecond precision and conversion
    start_time_pd = pd.Timestamp(start_time)
    timestamps_pd = pd.Series(timestamps)

    if start_time_pd > timestamps_pd[0]:
        raise err.TimestampsStartBeforeFileStartTime(
            "Your timestamps start before the file's global start time"
        )

    # Compute difference from global start time and convert to seconds
    # NWB requires times to be in seconds
    res = pd.Series(timestamps_pd - start_time_pd) / pd.Timedelta(
        value=1, unit="seconds"
    )
    return res.to_numpy()
