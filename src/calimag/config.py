"""Manage the configuration."""
from collections.abc import MutableMapping
from functools import lru_cache
from hashlib import md5
from pathlib import Path
from typing import Any, List

import pandas as pd
import toml

import calimag.constants as CONST
import calimag.errors as err
from calimag.parsers import MicroscopeParser


@lru_cache(maxsize=None)  # Keep config in cache since function called several times
def GetConfig(config_filepath: Path) -> MutableMapping[str, Any]:
    """Parse config file.

    - Checks for missing values or undefined paths.
    - Adds some defaults values where needed.
    """
    config = toml.load(config_filepath)
    assert check_mandatory_fields(node=config)

    # DataInput
    DataInput = config.get("DataInput")
    if not DataInput:
        raise err.ConfigMissingItem("DataInput section in the config file is missing")
    for key, value in DataInput.items():

        # Check supported experiment version
        if key == "experiment_version_mandatory":
            if value not in CONST.EXPERIMENT_VERSIONS:
                raise err.UnsupportedVersion(
                    f"The version `{value}` of your experiment "
                    "is unsupported, the currently supported versions are the "
                    f"following: {CONST.EXPERIMENT_VERSIONS}."
                )
            continue

        # Handle path-related config inputs
        # Convert paths strings to pathlib objects
        if "path" in (k := key.lower()) or "directory" in k:
            path = Path(value.strip())
            if key != "nwb_output_filepath_mandatory":
                assert check_path_fields(key, path)

            config["DataInput"][key] = path

    # Subject
    Subject = config.get("Subject")
    if Subject:
        subject_new: dict = {}
        for key, item in Subject.items():
            # Skip if empty string
            if not item:
                continue
            # Only copy line from Dict config if filled in or not empty str
            if key == "subject_id":
                # NWB method needs a str
                subject_new["subject_id"] = str(item)
            elif key == "weight":
                # NWB method needs a str
                subject_new["weight"] = str(item)
            elif key == "date_of_birth":
                # NWB method needs a datetime object
                # Convert to datetime if time is not provided in the config
                NWBFileSection = config.get("NWBFile")
                if NWBFileSection:
                    timezone = NWBFileSection.get("timezone")
                    if timezone is None or timezone == "":
                        timezone = CONST.TIMEZONE
                    # TODO: Raise error if the date of birth field has a timezone?
                    subject_new["date_of_birth"] = pd.Timestamp(item).tz_localize(
                        timezone
                    )

            else:
                # Just copy every other field
                subject_new[key] = item

        config["Subject"] = subject_new

    # NWBFile
    # Inject other metadata
    microscope_filepath = config["DataInput"]["microscope_filepath_mandatory"]
    parser = MicroscopeParser(filepath=microscope_filepath)
    session_start_time = parser.get_session_start_time()
    config["NWBFile"]["identifier"] = md5(
        str(session_start_time).encode("utf8")
    ).hexdigest()
    config["NWBFile"]["session_start_time"] = session_start_time

    # Imaging
    ImagingSection = config.get("Imaging")
    if not ImagingSection:
        raise err.ConfigMissingItem("Imaging section in the config file is missing")
    emission_lambda = ImagingSection.get("emission_lambda_mandatory")
    if emission_lambda:
        config["Imaging"]["emission_lambda"] = float(emission_lambda)

    return config


def check_mandatory_fields(node: dict) -> bool:
    """Raise an error if any mandatory field is missing."""
    for key, item in node.items():
        if isinstance(item, dict):  # If dict is a node
            check_mandatory_fields(item)

        else:  # If dict is a leaf
            if key.endswith("_mandatory") and item in [None, ""]:
                raise err.ConfigMissingItem(
                    f"Mandatory field '{key}' in the config file is missing"
                )
    return True


def check_path_fields(field: str, path: Path) -> bool:
    """Check path field satisfies convention (of only existing paths).

    Conventions:
    - Paths to specific files are signified by "*filepath*" in the field's name
    - Paths to directories are signified by "*directory*" in the field's name
    Returns True only if files exist and these 2 conditions are satisfied
    """
    field = field.lower()
    valid = False
    if "filepath" in field:
        valid = path.is_file()  # also checks for exists
        if not valid:
            raise FileNotFoundError(
                f"The path field '{field}' needs to be a path to an existing FILE."
                "Please check if this path exists and it is a file."
            )
    elif "directory" in field:
        valid = path.is_dir()  # also checks for exists
        if not valid:
            raise FileNotFoundError(
                f"The path field '{field}' needs to be a path to an existing DIRECTORY."
                "Please check if this path exists and it is a directory."
            )
    else:
        raise ValueError(f"The field to check {field} needs to be path-related inputs.")
    return valid


def remove_undefined_config_args(func_args: dict) -> dict:
    """Remove non mandatory config fields that are undefined.

    It should be safe to remove any undefined field as
    undefined mandatory fields are taken care of before
    in the `check_mandatory_fields()` function.
    """
    new_args = func_args.copy()
    for key in func_args.keys():
        if func_args.get(key) is None:
            new_args.pop(key)
    return new_args


def check_empty_fields(config: dict) -> List[str]:
    """Check if the user forgot any empty fields in the TOML config."""
    missing_fields: List[str] = []
    path_tree = ""

    def recurse_config(
        node: dict, missing_fields: List[str], path_tree: str
    ) -> List[str]:
        for key, item in node.items():
            path_tree = f"{key}" if path_tree == "" else f"{path_tree}.{key}"

            # If dict is a node
            if isinstance(item, dict):
                missing_fields = recurse_config(item, missing_fields, path_tree)
                continue

            # If it's a leaf
            assert isinstance(node.get(key), List)
            new_node = node.get(key)
            if new_node is None:
                break

            for field in new_node:
                nodes = path_tree.split(".")

                # If dict is a node
                if isinstance(field, dict):
                    missing_fields = recurse_config(field, missing_fields, path_tree)
                    continue

                # Test if those reference fields exist in the current config
                try:
                    for idx, no in enumerate(nodes):
                        if idx == 0:
                            config_field = config[no]
                        else:
                            config_field = config_field[no]
                    config_field = config_field[field]
                except KeyError:
                    missing_fields.append(f"{path_tree}.{field}")

            path_tree = ""
        return missing_fields

    missing_fields = recurse_config(CONST.CONFIG_REFERENCE, missing_fields, path_tree)
    return missing_fields
